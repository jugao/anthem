#ifndef VBFllvvHM_H
#define VBFllvvHM_H
#include "NtupleMaker.h"
#include "tools.h"
#include <math.h>

class VBFllvvHM : public NtupleMaker{
public:
//	using NtupleMaker::NtupleMaker;
	VBFllvvHM(string Input,bool SampleType);
	~VBFllvvHM(){};
	double FileNormSF;
	bool doVBFSel;
	void ReadTreeSkimed(TTree* sTree);
	void LoopEMuTau(TTree* sTree, string inName);
	void LoopEMuTau(TTree* sTree, TFile *InputFile,string inName);
	void Loop3lCR(TTree* sTree, string inName);
	void RunProcessor();
	void RunProcessor(string treename);
	void RunProcessor3lCR();
	double GetNormSF(int run, string FileName,string OutFileName);
	double GetNormSF(int run, TFile *InputFile,string FileName );
	void NormPlot( MakeHist* InClass, double NSF);
	void AddPlot(MakeHist*Base,MakeHist*Indep);
	void WritePlot(MakeHist*Base, TFile*OutFile);
	void WriteCutFlow(MakeHist*Base, TFile*OutFile);
	TTree *skimTree;
	TFile *skimFile;




	MakeHist* VBFPlot;
	MakeHist* VBFPlotee;
	MakeHist* VBFPlotmumu;
	MakeHist* ETauPlot;
	MakeHist* EMuPlot;
	MakeHist* MuTauPlot;
//	vector<string> InputSkimFileList;
	MakeHist* ETauPlotTotal;
	MakeHist* EMuPlotTotal;
	MakeHist* MuTauPlotTotal;
	MakeHist* VBFPlotTotal;
	MakeHist* VBFPloteeTotal;
	MakeHist* VBFPlotmumuTotal;
	MakeHist* VBFPlotAllChTotal;

	MakeHist* VBF3lCReee;
	MakeHist* VBF3lCReemu;
	MakeHist* VBF3lCRmumue;
	MakeHist* VBF3lCRmumumu;

	MakeHist* VBF3lCReeeTotal;
	MakeHist* VBF3lCReemuTotal;
	MakeHist* VBF3lCRmumueTotal;
	MakeHist* VBF3lCRmumumuTotal;
	MakeHist* VBF3lCRAllChTotal;

        map<string,MakeHist*> MCMH3lCRTotal = {
		{"mumumu",VBF3lCRmumumuTotal},
		{"mumue",VBF3lCRmumueTotal},
		{"eee",VBF3lCReeeTotal},
		{"eemu",VBF3lCReemuTotal},
		{"AllCh",VBF3lCRAllChTotal}
	};

        map<string,MakeHist*> MCMH3lCR = {
		{"mumumu",VBF3lCRmumumu},
		{"mumue",VBF3lCRmumue},
		{"eee",VBF3lCReee},
		{"eemu",VBF3lCReemu}
	};


	map<string,MakeHist*> VBFPTotal = {
		{"mumu",VBFPlotmumuTotal},
		{"ee",VBFPloteeTotal},
		{"AllCh",VBFPlotAllChTotal}
	};

	map<string,MakeHist*> VBFP= {
		{"mumu",VBFPlotmumu},
		{"ee",VBFPlotee}
	};

	int year;
	double ChannelNumber;
	double lum;

   // Declaration of leaf types
   Int_t           run;
   Int_t           mcRandomRun;
   Int_t           bcid;
   ULong64_t       event;
   ULong64_t       PRWHash;
   Int_t           isMC;
   Int_t           mu;
   Int_t           npv;
   Int_t           event_type;
   Int_t           event_3CR;
   Int_t           SR_HM_LM;
   Float_t         weight_pileup;
   Float_t         weight_gen;
   Float_t         weight_exp;
   Float_t         weight_trig;
   Float_t         weight_jets;
   Float_t         weight_jvt;
   Float_t         weight;
   Float_t         lepplus_pt;
   Float_t         lepminus_pt;
   Float_t         lepplus_eta;
   Float_t         lepminus_eta;
   Float_t         lepplus_phi;
   Float_t         lepminus_phi;
   Float_t         lepplus_m;
   Float_t         lepminus_m;
   Int_t           medium_3rd;
   Int_t           charge_3rd;
   Float_t         lep3rd_pt;
   Float_t         lep3rd_eta;
   Float_t         lep3rd_phi;
   Float_t         lep3rd_m;
   Float_t         leading_pT_lepton;
   Float_t         subleading_pT_lepton;
   Float_t         electron_pt;
   Float_t         muon_pt;
   Float_t         electron_eta;
   Float_t         muon_eta;
   Int_t           n_jets;
   Int_t           n_cjets;
   Int_t           n_fjets;
   Float_t         pTjet_flead;
   Int_t           n_fjvtjets;
   Int_t           n_bjets;
   Float_t         leading_jet_pt;
   Float_t         leading_jet_eta;
   Float_t         leading_jet_rapidity;
   Float_t         second_jet_pt;
   Float_t         second_jet_eta;
   Float_t         second_jet_rapidity;
   Float_t         leading_twojets_vsum_pt;
   Float_t         jet_sumpt;
   Float_t         jet_vsum_pt;
   Float_t         jet_vsum_eta;
   Float_t         jet_vsum_phi;
   Float_t         jet_vsum_m;
   Float_t         mjj;
   Float_t         detajj;
   Float_t         max_mjj;
   Float_t         max_detajj;
   Float_t         cos_Z_2jets;
   Float_t         central_Z_2jets;
   Float_t         jet_central_ST;
   Float_t         jet30_central_ST;
   Float_t         jet40_central_ST;
   Float_t         jet50_central_ST;
   Int_t           n_central_jets;
   Int_t           n_central_jets30;
   Int_t           n_central_jets40;
   Int_t           n_central_jets50;
   Float_t         m_leading_jet_pt;
   Float_t         m_leading_jet_eta;
   Float_t         m_leading_jet_rapidity;
   Float_t         m_second_jet_pt;
   Float_t         m_second_jet_eta;
   Float_t         m_second_jet_rapidity;
   Int_t           m_n_central_jets;
   Float_t         m_leading_twojets_vsum_pt;
   Float_t         dPhiMET_MLeadJet;
   Float_t         dPhiMET_MSecJet;
   vector<float>   *jets_pt;
   vector<float>   *jets_eta;
   vector<float>   *jets_phi;
   vector<float>   *jets_m;
   Float_t         met_tst;
   Float_t         met_px_tst;
   Float_t         met_py_tst;
   Float_t         met_tst_loose;
   Float_t         met_px_tst_loose;
   Float_t         met_py_tst_loose;
   Float_t         met_soft;
   Float_t         met_px_soft;
   Float_t         met_py_soft;
   Float_t         met_jets_pt;
   Float_t         met_muon_pt;
   Float_t         met_electron_pt;
   Float_t         dphi_met_jets;
   Float_t         met_signi;
   Float_t         met_signif;
   Float_t         mT_ZZ;
   Float_t         Z_eta;
   Float_t         Z_pT;
   Float_t         Z_rapidity;
   Float_t         met_obj;
   Float_t         mZZ_Truth;
   Float_t         rho_Truth;
   Float_t         ZpTomT;
   Float_t         M2Lep;
   Float_t         dLepR;
   Float_t         dMetZPhi;
   Float_t         frac_pT;
   Float_t         dPhiJ25met;
   Float_t         dPhiJ100met;
   Float_t         mT_Hinv;
   Float_t         ZpTomT_Hinv;
   Float_t         MetOHT;
   Float_t         sumpT_scalar;




};
#endif
