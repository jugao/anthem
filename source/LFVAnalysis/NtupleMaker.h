#ifndef ntupleProcessor_H
#define ntupleProcessor_H
#include <vector>
#include "TLorentzVector.h"
#include <TRandom3.h>
#include <TMinuit.h>
#include <TROOT.h>
#include <TTree.h>
#include <TChain.h>
#include <TFile.h>
#include <TEntryList.h>
#include <string>
#include <vector>
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include "TMath.h"
#include <math.h>
#include <map>
#include "THStack.h"
#include <string>
#include "TCanvas.h"
#include "TLegend.h"
#include "TColor.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TKey.h"

static const double Pi = 3.14159265;
static const double ElePDGMass = 0.511*0.001;
static const double MuonPDGMass = 105.658367*0.001;
static const double TauPDGMass = 1776.82*0.001;
static const double GeV = 0.001;
void string_replace( std::string &strBig, const std::string &strsrc, const std::string &strdst);
std::string GetPathOrURLShortName(std::string strFullName);


double CaldPhi(double phi1, double phi2);

    using namespace std;

    class NtupleMaker {
      public:
//    struct LFVVarible {
        string m_outputName;//!


        //Meta
        string *filename; //!
        double sumOfWeights; //!
        int nEventsProcessed;//!
        //event Info
        double EventWeight;//!
        double PrwWeight;//!
        double MCWeight;//!
        double totalEvents;//!
        bool Event_Trigger_Pass;//!
        bool Event_Trigger_Match;//!
        int eventNumber; //!
        int mcChannelNumber;//!
        int EventTag;//!
        int RunNo;//!
        double lumB;//!
        double xsec;//!
        double kFactor;//!
        double dPhi_ej;//!
        double dPhi_ll;//!
        bool isKeepEvent; //!
        bool isEvtClean; //!
        bool isVertexGood; //!
        bool isTrigPass; //!
        int N_jets;//!
        int m_numCleanEvents;//!

        //Missing ET Info
        double MET_SumEt; //!
        double MET_Py; //!
        double MET_Px; //!
        double MET_Phi; //!
        double MET_Et; //!
        double Event_met; //!
        double Event_met_eta; //!
        double Event_met_phi; //!
        double Event_met_m; //!
        double Event_mT; //!
        //Electron para
        std::vector<int>    *Ele_Charge=0;       //!
        std::vector<double> *Ele_pt=0;       //!
        std::vector<double> *Ele_eta=0;      //!
        std::vector<double> *Ele_eta_calo=0; //!
        std::vector<double> *Ele_phi=0;      //!
        std::vector<double> *Ele_d0sig=0;    //!
        std::vector<double> *Ele_dz0=0;    //!
        std::vector<bool>   *Ele_OQ=0;      //!
        std::vector<bool>   *Ele_d0pass=0; //!
        std::vector<bool>   *Ele_z0pass=0; //!
        std::vector<bool>   *Ele_MuonOLR=0; //!
        std::vector<bool>   *Ele_ID_Medium=0;      //!
        std::vector<bool>   *Ele_ID_Tight=0;      //!
        std::vector<bool>   *Ele_ID_Loose=0;      //!
        std::vector<bool>   *Ele_Iso=0;      //!
        std::vector<bool>   *Ele_Iso_Loose=0;      //!
        std::vector<bool>   *Ele_isTrigMch=0; //!
        std::vector<double> *Ele_TrigEff=0;   //!
        std::vector<double> *Ele_RecoSF=0;   //!
        std::vector<double> *Ele_IDSF=0;     //!
        std::vector<double> *Ele_IsoSF=0;     //!
        std::vector<double> *Ele_L1CaloSF=0;     //!
        std::vector<double> *Ele_isOverlap=0;     //!
        //Muon Para
        std::vector<double> *Mu_pt=0; //!
        std::vector<double> *Mu_eta=0; //!
        std::vector<double> *Mu_phi=0; //!
        std::vector<double> *Mu_d0sig=0; //!
        std::vector<double> *Mu_dz0=0; //!
        std::vector<int>    *Mu_charge=0; //!
        std::vector<bool>   *Mu_d0pass=0; //!
        std::vector<bool> *Mu_z0pass=0; //!
        std::vector<bool> *Mu_Iso=0; //!
        std::vector<bool> *Mu_isHighPt=0;  //!
        std::vector<bool> *Mu_isID=0;  //!
        std::vector<bool> *Mu_isTrigMch=0;  //!
        std::vector<double> *Mu_TrigSF=0;   //!
        std::vector<double> *Mu_RecoSF=0;   //!
        std::vector<double> *Mu_TrackSF=0;   //!
        std::vector<double> *Mu_IsoSF=0;    //!
        std::vector<bool> *Mu_isOverlap=0;     //!

        //Tau Para
        std::vector<double> *Tau_pt=0;     //!
        std::vector<double> *Tau_eta=0;     //!
        std::vector<double> *Tau_phi=0;     //!
        std::vector<int> *Tau_charge=0;     //!
        std::vector<int> *Tau_ntracks=0;     //!
        //  std::vector<bool> Tau_MuonOLR;     //!
        // std::vector<bool> Tau_EleOLR;     //!
        std::vector<bool> *Tau_isOverlap=0;     //!
        std::vector<bool> *TauSelector=0;     //!
        std::vector<bool> *TauSelector_WithOLR=0;     //!
        std::vector<double> *Tau_RecoSF=0; //!
        std::vector<double> *Tau_IDSF=0; //!
        std::vector<double> *Tau_EleOLRSF=0; //!

        //Jet Para
        std::vector<double> *Jet_pt=0;     //!
        std::vector<double> *Jet_eta=0;     //!
        std::vector<double> *Jet_phi=0;     //!
        std::vector<double> *Jet_m=0;     //!
        std::vector<bool>   *Jet_isGoodB=0;     //!
        std::vector<double> *Jet_JVTSF=0; //!
        std::vector<double> *Jet_BTInefSF=0; //!
        std::vector<double> *Jet_BTSF=0; //!

        //Selection Tag
        bool m_electronMuon; //!
        bool m_electronTau; //!
        bool m_muonTau; //!
        bool Ele_TriggerMatched;//!
        bool Mu_TriggerMatched;//!
        std::vector<bool> *Ele_isTight=0;//!
        std::vector<bool> *Ele_isLoose=0;//!
        std::vector<bool> *Ele_isLoose_IDLoose=0;//!
        //CutFlow
        TH1D *h_CleanEventNum; //!
        TH1D *h_CutFlow_emu; //!
        TH1D *h_CutFlow_etau; //!
        TH1D *h_CutFlow_mutau; //!
        TH1D *h_CutFlow_el; //!
        TH1D *h_CutFlow_muon; //!
        TH1D *h_CutFlow_tau; //!
        TH1D *h_CutFlow_jet; //!
        TH1D *h_CutFlow_ph; //!
//   };

//    LFVVarible Dilep;
        string inputlist;
        string OutPutName;
        double CalNMSF;
        vector<string> InputFileList;
        string InPutFile;
        string outputname;
        TTree *EMuTauTree;
//    TTree *ssTree;
//    TChain *ssTree;
        TChain *meta;
	double DPhi_ll;
        double ele_pt;
        double ele_eta;
        double ele_phi;
        bool isData;
        bool ele_isTight;
        double weight;
        double mu_pt;
        double mu_eta;
        double mu_phi;
        bool mu_isTight;
        double tau_pt;
        double tau_eta;
        double tau_phi;
        bool tau_isTight;
        double xsecSF;
        double lumSF;
        double NormSF;
        int Etag;
        double luminosity;
        string wrkarea;
	double muonTrigSF;
	double muonRecoSF;
	double muonIsoSF;
	double muonTtvaSF;
	double electronTrigSF;
	double electronIDSF;
	double electronRecoSF;
	double electronIsoSF;
	double electronL1CaloSF;
	double tauRecoSF;
	double tauEleOlrSF;
	double tauEJetIDSF;
	double jetJvtSF;

        void SkimProcessor( string inFileName, TChain *chain);
        void ReadTreeLFVO(TChain *chain);

        double FindXsec(double mcChannelNumber);
        void DrawMass(string inFileName, string outFlieName);
        double CalLumNormSF(TChain *chain, double xSec);
        double CalLumNormSF(double MCRun);
        void RunSkim(void);
        void InitialVarible();
	double Lum(string INST);





        NtupleMaker(string Input,bool SampleType);
        virtual    ~NtupleMaker() {};







    };
#endif
