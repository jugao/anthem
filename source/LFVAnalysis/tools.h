#ifndef tools_H
#define tools_H
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <unordered_map>
#include <sstream>
#include <string.h>
#include <errno.h>
#include "TCanvas.h"
#include "TDatime.h"
#include "TGraph.h"
#include "TH1F.h"
#include "TH2.h"
#include "TBox.h"
#include "TText.h"
#include "TLatex.h"
#include "THStack.h"
#include "TLine.h"
#include "TStyle.h"
#include "TFrame.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMath.h"
#include "TROOT.h"
#include "TLegend.h"
#include "TGraphAsymmErrors.h"
#include "TApplication.h"
#include "TLorentzVector.h"
#include "TPad.h"

using namespace::std;

std::string convertDouble(double number);
std::string convertInt(double number);
double CalNorm(double lumiData, double xSec,double sumW);
//int add(int a,int b);
void MyStyle();
void plotGraph(string xx, vector<string> yy);
void ATLASLabel(Double_t x,Double_t y,const char* text,Color_t color);
void myText(Double_t x,Double_t y,Color_t color,const char *text, Double_t tsize=0.04);

class Plot {
  public:
    unordered_map<string,string> VBFHZZXTitle = {
        {"MET","E^{miss}_{T}[GeV]"},
        {"DeltaEtajj","#Delta#eta_{jj}"},
        {"Mjj","M_{jj}[GeV]"},
        {"METSig","MET_sig"},
        {"NumJets","N_jets"},
        {"NumBJets","N_Bjets"},
        {"PtLead","Pt_lead[GeV]"},
        {"PtSub","Pt_sub[GeV]"},
        {"dileRap","#DeltaR_{ll}"},
        {"lep1Phi","#Delta#phi(E^{miss}_{T},jets)"},
        {"lep2Phi","#Delta#phi(E^{miss}_{T},Pt^{ll})"},
        {"PFractPt","FractPt"},
        {"PMetOHT","E^{miss}_{T}/H_{T}"},
        {"Mll","M_{ll}[GeV]"},
        {"lep3Phi","#phi^{3rd}"},
        {"lep3Eta","#eta^{3rd}"},
        {"lep3Pt","Pt^{3rd}[GeV]"},
        {"MWT","m_{T}W"},
        {"ZMT","m_{T}ZZ"},
        {"dMetZPhi","#Delta#phi(E^{miss}_{T},Pt^{ll})"},
        {"dPhiJ100met","#Delta#phi(E^{miss}_{T},jets)"},
	{"dilePt","Pt^{ll}"},
	{"EMulep1Pt","Pt^{e}"},
	{"EMulep1Eta","#eta^{e}"},
	{"EMulep1Phi","#phi^{e}"},
	{"EMulep2Pt","Pt^{#mu}"},
	{"EMulep2Eta","#eta^{#mu}"},
	{"EMulep2Phi","#phi^{#mu}"},
	{"ETaulep1Pt","Pt^{e}"},
	{"ETaulep1Eta","#eta^{e}"},
	{"ETaulep1Phi","#phi^{e}"},
	{"ETaulep2Pt","Pt^{#tau}"},
	{"ETaulep2Eta","#eta^{#tau}"},
	{"ETaulep2Phi","#phi^{#tau}"},
	{"MuTaulep2Pt","Pt^{#tau}"},
	{"MuTaulep2Eta","#eta^{#tau}"},
	{"MuTaulep2Phi","#phi^{#tau}"},
	{"MuTaulep1Pt","Pt^{#mu}"},
	{"MuTaulep1Eta","#eta^{#mu}"},
	{"MuTaulep1Phi","#phi^{#mu}"},
    };

    unordered_map<string,string> LatexInfo = {
        {"1516","36.2 fb^{-1}"},
        {"17","44.3 fb^{-1}"},
        {"18","59.9 fb^{-1}"},
        {"1516A17","80.5 fb^{-1}"},
        {"15161718","140.4 fb^{-1}"}
    };
    Plot(char *fch = (char*)"test",char *Cch = (char*)"c1",int CL = 900, int CW = 900 );
    TCanvas *c1;
    TPad *pad1;
    TPad *pad3;
    TH1D *ratio;
    TH1D *ratio0;
    void plotDataMC(TH1D* data, TH1D* mc);
    void plot2D(TH1D* th1, TH1D* th2);
    void plotDataMCSTA(TH1D* data, THStack* mc,TLegend*legend);
    void plotDataMCSTA(TH1D* data, THStack* mc,TLegend*legend,vector<TH1D*> & vecT);
    ~Plot();
  private:
    char *fName;
    char *CanvasName;
    int CanvasWidth;
    int CanvasLength;

};


class SetBin {
  public:
    int nBins;
    double lowEdge;
    double highEdge;
    int nBins2;
    double lowEdge2;
    double highEdge2;
    double *arr;
    SetBin(int bin, double lowe,double highe);
    SetBin(int bin, double *binarr);
    SetBin(int bin1, double lowe1, double highe1,int bin2, double lowe2, double highe2);
    ~SetBin() {}

};

class MakeHist {
  public:
    MakeHist(string ObjName, string SName);
    string SSName;
    map<int, string> TMap= {{0,"T1"},{1,"T2"}};
    map<int, string> LFVHist={
	    {0,"lep1Pt"},
	    {1,"lep1Eta"},
	    {2,"lep1Phi"},
	    {3,"lep2Pt"},
	    {4,"lep2Eta"},
	    {5,"lep2Phi"},
	    {6,"dilePt"},
	    {7,"Mll"},
    };
    map<int,string> VBFHist2D={
	    {0,"METDeltajj"},
	    {1,"METMjj"},
	    {2,"METSigMjj"},
	    {3,"METSigDeltajj"},
    };

    map<int, string> VBFHist= {
        {0,"MET"},
        {1,"dileRap"},
        {2,"dMetZPhi"},
        {3,"dPhiJ100met"},
        {4,"NumBJets"},
        {5,"PFractPt"},
        {6,"PMetOHT"},
        {7,"METSig"},
        {8,"NumJets"},
        {9,"PtLead"},
        {10,"PtSub"},
        {11,"Mjj"},
        {12,"DeltaEtajj"},
        {13,"MWT"},
        {14,"lep3Pt"},
        {15,"lep3Eta"},
        {16,"lep3Phi"},
        {17,"Mll"},
        {18,"ZMT"},
    };



    vector<TH1D*> testlist;
    vector<TH1D*> THList;
    vector<TH2D*> THList2D;
//    vector<TH1D*> THList3lCR;

    TH1D *mass;
    TH1D *lep1Pt;
    TH1D *lep1Eta;
    TH1D *lep1Phi;
    TH1D *lep2Pt;
    TH1D *lep2Eta;
    TH1D *lep2Phi;
    TH1D *lep3Pt;
    TH1D *lep3Eta;
    TH1D *lep3Phi;
    TH1D *dilePt;
    TH1D *dileRap;
    TH1D *Mjj;
    TH1D *DeltaEtajj;
    TH1D *MET;
    TH1D *METSig;
    TH1D *NumJets;
    TH1D *NumBJets;
    TH1D *PtLead;
    TH1D *PtSub;
    TH1D *MWT;

    TH1D *CutFlow1;
    TH1D *CutFlow2;
    TH1D *CutFlow3;
    TH1D *CutFlow4;
    TH1D *CutFlow5;


    TH1D *PFractPt;
    TH1D *PMetOHT;

    void MKTH1D();
    void MKTH2D();
    void ReadCutFlow(TH1D *cut, string Cate = " ");
    void ReadCutFlowSum(TH1D *cut, string Cate = " ");
    void MKCutFlow();
    map<std::string, SetBin*> Binning;
    TH1D *DrawDilp(string option,string binName, TLorentzVector l1, TLorentzVector l2,double weight = 1);
    void DeleteTH1D();

    virtual ~MakeHist() {}
//  private:
    string OName;
    double SNbin;
    double SNL;
    double SNH;

};

class MakeTHStack : public MakeHist {
  public:
    MakeTHStack(string ObjName,string SName);

    TLegend *legend;
    THStack *Smass;
    THStack *Slep1Pt;
    THStack *Slep1Eta;
    THStack *Slep1Phi;
    THStack *Slep2Pt;
    THStack *Slep2Eta;
    THStack *Slep2Phi;
    THStack *Slep3Pt;
    THStack *Slep3Eta;
    THStack *Slep3Phi;
    THStack *SdilePt;
    THStack *SdileRap;

    THStack *SMjj;
    THStack *SDeltaEtajj;
    THStack *SMET;
    THStack *SMWT;
    THStack *SZMT;
    THStack *SMETSig;
    THStack *SNumJets;
    THStack *SNumBJets;
    THStack *SPtLead;
    THStack *SPtSub;
    THStack *SPFractPt;
    THStack *SPMetOHT;

    vector<TH1D*> SGD;

    vector<THStack*> THStackVBF;
    vector<THStack*> THStackLFV;


    map<string,THStack*> MCStack = {
        {"MET",SMET},
        {"MWT",SMWT},
        {"ZMT",SZMT},
        {"METSig",SMETSig},
        {"Mjj",SMjj},
        {"DeltaEtajj",SDeltaEtajj},
        {"NumJets",SNumJets},
        {"NumBJets",SNumBJets},
        {"PtLead",SPtLead},
        {"PtSub",SPtSub},
        {"dileRap",SdileRap},
        {"lep1Phi",Slep1Phi},
        {"lep2Phi",Slep2Phi},
        {"lep3Pt",Slep3Pt},
        {"lep3Eta",Slep3Eta},
        {"lep3Phi",Slep3Phi},
        {"PFractPt",SPFractPt},
        {"PMetOHT",SPMetOHT}
    };

    void ReadFile(string FileName);
    void DrawStack(string dataFile,string DrawOption,bool drawSignal =0);

};


#endif
