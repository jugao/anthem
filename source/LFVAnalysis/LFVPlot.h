#ifndef LFVANA_H
#define LFVANA_H
#include "NtupleMaker.h"
#include "tools.h"

class LFVANA : public NtupleMaker{
public:
//	using NtupleMaker::NtupleMaker;
	LFVANA(string Input,bool SampleType);
	~LFVANA(){};
	double FileNormSF;
	void ReadTreeSkimed(TTree* sTree);
	void LoopEMuTau(TTree* sTree ,string inName);
	void RunProcessor();
	void NormPlot( MakeHist* InClass, double NSF);
	void AddPlot(MakeHist*Base,MakeHist*Indep);
	void WritePlot(MakeHist*Base, TFile*OutFile);
	TTree *skimTree;
	TFile *skimFile;
	bool isMC;
	bool PAC;

	MakeHist* LFVETauPlot;
	MakeHist* LFVEMuPlot;
	MakeHist* LFVMuTauPlot;
//	vector<string> InputSkimFileList;
	MakeHist* LFVETauPlotTotal;
	MakeHist* LFVEMuPlotTotal;
	MakeHist* LFVMuTauPlotTotal;
	MakeHist* LFVAllChPlotTotal;

	map<string, MakeHist*> LFVMHTotal = {
		{"ETau",LFVETauPlotTotal},
		{"EMu",LFVEMuPlotTotal},
		{"MuTau",LFVMuTauPlotTotal},
		{"AllCh",LFVAllChPlotTotal}
 	};
	map<string, MakeHist*> LFVMH = {
		{"ETau",LFVETauPlot},
		{"EMu",LFVEMuPlot},
		{"MuTau",LFVMuTauPlot}
	};

  
};
#endif
