#include "LFVPlot.h"

LFVANA::LFVANA(string Input,bool SampleType):NtupleMaker(Input,SampleType) {
    wrkarea = "/eos/user/j/jugao/LFVWORK/SkimedRoot/Sample/";
    PAC = !SampleType;
    cout <<"SAM="<<SampleType<<endl;
    cout <<"PAC="<<PAC<<endl;
//    string Inos;
//    ifstream os;
//    os.open(Input.c_str());
//    while(os >> Inos) {
//        InputFileList.push_back(Inos);
//    }
//    if ( Input.length() > 20) {
//        Input.erase(0,33);
//    }
//    filename = 0;
//    Input.erase(Input.length()-5);
//    OutPutName = Input;

}


void LFVANA::ReadTreeSkimed( TTree * sTree ) {
    sTree->SetBranchAddress("ele_pt",&ele_pt);
    sTree->SetBranchAddress("ele_eta",&ele_eta);
    sTree->SetBranchAddress("ele_phi",&ele_phi);
    sTree->SetBranchAddress("ele_isTight",&ele_isTight);
    sTree->SetBranchAddress("mu_pt",&mu_pt);
    sTree->SetBranchAddress("mu_eta",&mu_eta);
    sTree->SetBranchAddress("mu_phi",&mu_phi);
    sTree->SetBranchAddress("mu_isTight",&mu_isTight);
    sTree->SetBranchAddress("tau_pt",&tau_pt);
    sTree->SetBranchAddress("tau_eta",&tau_eta);
    sTree->SetBranchAddress("tau_phi",&tau_phi);
    sTree->SetBranchAddress("tau_isTight",&tau_isTight);
    sTree->SetBranchAddress("isData",&isData);
    sTree->SetBranchAddress("NormSF",&NormSF);
    sTree->SetBranchAddress("weight",&weight);
    sTree->SetBranchAddress("Etag",&Etag);
}

void LFVANA::LoopEMuTau(TTree* sTree,string inName ) {
    for(int ii=0; ii<sTree->GetEntries(); ii++) {
        sTree->GetEntry(ii);
        if (ii ==0) cout <<isData<<endl;
//        isMC =1;
//        if ((inName.find("Data") != string::npos)||(inName.find("DATA") != string::npos)) isMC = 0;
//        cout<<ii<<endl;
        FileNormSF = NormSF;
        string Type = "";
        TLorentzVector le1(1,1,1,1);
        TLorentzVector le2(1,1,1,1);

        if (Etag == 1) {
            Type = "ETau";
        } else if (Etag == 0) {
            Type = "EMu";
        } else if (Etag == 2) {
            Type = "MuTau";
        }
        if (Type == "ETau") {
		if (!ele_isTight) continue;

            le1.SetPtEtaPhiM(ele_pt,ele_eta,ele_phi,ElePDGMass);
            le2.SetPtEtaPhiM(tau_pt,tau_eta,tau_phi,TauPDGMass);
	    cout <<"ETweight="<<weight<<endl;
	    cout <<"ETNormSF"<<NormSF<<endl;

            if (mu_pt !=0) cout<<"mu in Etau&"<<"pt="<<mu_pt<<endl;
        }
        if (Type == "EMu") {
		if (!ele_isTight) continue;
		if (!mu_isTight) continue;
            le1.SetPtEtaPhiM(ele_pt,ele_eta,ele_phi,ElePDGMass);
            le2.SetPtEtaPhiM(mu_pt,mu_eta,mu_phi,MuonPDGMass);
            if (tau_pt !=0) cout<<"tau in EMu&"<<"pt="<<tau_pt<<endl;
        }
        if (Type == "MuTau") {
		if (!mu_isTight) continue;

            le1.SetPtEtaPhiM(mu_pt,mu_eta,mu_phi,MuonPDGMass);
            le2.SetPtEtaPhiM(tau_pt,tau_eta,tau_phi,TauPDGMass);
	    cout <<"MuTweight="<<weight<<endl;
	    cout <<"MuTNormSF"<<NormSF<<endl;
            if (ele_pt !=0) cout<<"ele in MuTau&"<<"pt="<<ele_pt<<endl;
	    if((le1+le2).M() <130)  cout << (le1+le2).M()<<endl;
        }

	
//        cout <<weight<<endl;
        LFVMH[Type]->THList[7]->Fill((le1+le2).M(),weight);
        LFVMH[Type]->THList[0]->Fill((le1).Pt(),weight);
        LFVMH[Type]->THList[1]->Fill((le1).Eta(),weight);
        LFVMH[Type]->THList[2]->Fill((le1).Phi(),weight);
        LFVMH[Type]->THList[3]->Fill((le2).Pt(),weight);
        LFVMH[Type]->THList[4]->Fill((le2).Eta(),weight);
        LFVMH[Type]->THList[5]->Fill((le2).Phi(),weight);
        LFVMH[Type]->THList[6]->Fill((le1+le2).Pt(),weight);

    }
}


void LFVANA::NormPlot( MakeHist* InClass, double NSF) {
    for (auto vec = (InClass->THList).begin(); vec < (InClass->THList).end(); vec++) {
        (*vec)->Scale(NSF);
    }

}

void LFVANA::AddPlot(MakeHist*Base,MakeHist*Indep) {
    for (size_t ii = 0; ii < ((Base->THList).size()); ii++) {
        Base->THList[ii]->Add(Indep->THList[ii]);
    }
}


void LFVANA::WritePlot(MakeHist*Base, TFile*OutFile) {
    for (auto vec = (Base->THList).begin(); vec < (Base->THList).end(); vec++) {
        (*vec)->Write();
    }
}


void LFVANA::RunProcessor() {
    TFile* outputfile = new TFile((OutPutName+"Plots.root").c_str(),"recreate");
    typedef std::vector<std::string>::iterator STRING_ITOR;
    STRING_ITOR inF;
    FileNormSF = 0;
    for (auto& x :LFVMHTotal) {
        x.second = new MakeHist("LFVMHT"+x.first,OutPutName);
        x.second->MKTH1D();
        x.second->MKCutFlow();
    }
//    isMC = 0;

    int Itag=0;
    for (inF = InputFileList.begin(); inF != InputFileList.end(); ++inF) {
        Itag++;
        TFile* testF = new TFile((*inF).c_str(),"READ");
        TTree* SkimTree = (TTree*)testF->Get("Emutau");
        for (auto& xx:LFVMH ) {
            xx.second = new MakeHist("LFVMH"+xx.first+convertDouble(Itag),OutPutName);
            xx.second->MKTH1D();
        }

        cout<<"loop file:"<<(*inF).c_str()<<endl;

        ReadTreeSkimed(SkimTree);
        LoopEMuTau(SkimTree, (*inF));
    cout <<"PAC2="<<PAC<<endl;
	isMC = PAC;
	cout <<isMC<<endl;
        if(isMC) {
            for(auto& yy :LFVMH) {
		    cout << FileNormSF<<endl;
                NormPlot(yy.second,FileNormSF);
            }
        }

        for (auto& zz :LFVMH) {
            AddPlot(LFVMHTotal[zz.first],zz.second);
            delete zz.second;
        }
        delete testF;
    }
    for ( auto& zt : LFVMHTotal ) {
        AddPlot(LFVMHTotal["AllCh"],LFVMHTotal[zt.first]);
//	    LFVMHTotal["AllCh"]->CutFlow1->
    }
    outputfile->cd();

    for (auto& xn : LFVMHTotal) {
        WritePlot(xn.second,outputfile);
    }
    outputfile->Close();

}
