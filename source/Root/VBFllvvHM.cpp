#include "VBFllvvHM.h"
#include "getxsec.h"


VBFllvvHM::VBFllvvHM(string Input,bool SampleType):NtupleMaker(Input,SampleType) {
    wrkarea = "/eos/user/j/jugao/VBFllvvHM/";
    doVBFSel = SampleType;
//    string Inos;
//    ifstream os;
//    isData = SampleType;
//    os.open(Input.c_str());
//    while(os >> Inos) {
//        InputFileList.push_back(Inos);
//    }
//    if ( Input.length() > 20) {
//        Input.erase(0,33);
//    }
//    filename = 0;
//    Input.erase(Input.length()-5);
//    OutPutName = Input;

}


void VBFllvvHM::ReadTreeSkimed( TTree * sTree ) {
    sTree->SetBranchAddress("run",&run);
    sTree->SetBranchAddress("mcRandomRun",&mcRandomRun);
    sTree->SetBranchAddress("bcid",&bcid);
    sTree->SetBranchAddress("event",&event);
    sTree->SetBranchAddress("PRWHash",&PRWHash);
    sTree->SetBranchAddress("isMC",&isMC);
//    sTree->SetBranchAddress("mu",&mu);
//    sTree->SetBranchAddress("npv",&npv);
    sTree->SetBranchAddress("event_type",&event_type);
    sTree->SetBranchAddress("event_3CR",&event_3CR);
    sTree->SetBranchAddress("SR_HM_LM",&SR_HM_LM);
    sTree->SetBranchAddress("weight_pileup",&weight_pileup);
    sTree->SetBranchAddress("weight_gen",&weight_gen);
    sTree->SetBranchAddress("weight_exp",&weight_exp);
    sTree->SetBranchAddress("weight_trig",&weight_trig);
    sTree->SetBranchAddress("weight_jets",&weight_jets);
    sTree->SetBranchAddress("weight_jvt",&weight_jvt);
    sTree->SetBranchAddress("weight",&weight);
    sTree->SetBranchAddress("lepplus_pt",&lepplus_pt);
    sTree->SetBranchAddress("lepminus_pt",&lepminus_pt);
    sTree->SetBranchAddress("lepplus_eta",&lepplus_eta);
    sTree->SetBranchAddress("lepminus_eta",&lepminus_eta);
    sTree->SetBranchAddress("lepplus_phi",&lepplus_phi);
    sTree->SetBranchAddress("lepminus_phi",&lepminus_phi);
    sTree->SetBranchAddress("lepplus_m",&lepplus_m);
    sTree->SetBranchAddress("lepminus_m",&lepminus_m);
    sTree->SetBranchAddress("medium_3rd",&medium_3rd);
    sTree->SetBranchAddress("charge_3rd",&charge_3rd);
    sTree->SetBranchAddress("lep3rd_pt",&lep3rd_pt);
    sTree->SetBranchAddress("lep3rd_eta",&lep3rd_eta);
    sTree->SetBranchAddress("lep3rd_phi",&lep3rd_phi);
    sTree->SetBranchAddress("lep3rd_m",&lep3rd_m);
    sTree->SetBranchAddress("leading_pT_lepton",&leading_pT_lepton);
    sTree->SetBranchAddress("subleading_pT_lepton",&subleading_pT_lepton);
//    sTree->SetBranchAddress("electron_pt",&electron_pt);
//    sTree->SetBranchAddress("muon_pt",&muon_pt);
//    sTree->SetBranchAddress("electron_eta",&electron_eta);
//    sTree->SetBranchAddress("muon_eta",&muon_eta);
    sTree->SetBranchAddress("n_jets",&n_jets);
//    sTree->SetBranchAddress("n_cjets",&n_cjets);
//    sTree->SetBranchAddress("n_fjets",&n_fjets);
//    sTree->SetBranchAddress("pTjet_flead",&pTjet_flead);
//    sTree->SetBranchAddress("n_fjvtjets",&n_fjvtjets);
    sTree->SetBranchAddress("n_bjets",&n_bjets);
    sTree->SetBranchAddress("leading_jet_pt",&leading_jet_pt);
    sTree->SetBranchAddress("leading_jet_eta",&leading_jet_eta);
//    sTree->SetBranchAddress("leading_jet_rapidity",&leading_jet_rapidity);
    sTree->SetBranchAddress("second_jet_pt",&second_jet_pt);
    sTree->SetBranchAddress("second_jet_eta",&second_jet_eta);
//    sTree->SetBranchAddress("second_jet_rapidity",&second_jet_rapidity);
//    sTree->SetBranchAddress("leading_twojets_vsum_pt",&leading_twojets_vsum_pt);
    sTree->SetBranchAddress("jet_sumpt",&jet_sumpt);
//    sTree->SetBranchAddress("jet_vsum_pt",&jet_vsum_pt);
//    sTree->SetBranchAddress("jet_vsum_eta",&jet_vsum_eta);
//    sTree->SetBranchAddress("jet_vsum_phi",&jet_vsum_phi);
//    sTree->SetBranchAddress("jet_vsum_m",&jet_vsum_m);
    sTree->SetBranchAddress("mjj",&mjj);
    sTree->SetBranchAddress("detajj",&detajj);
//    sTree->SetBranchAddress("max_mjj",&max_mjj);
//    sTree->SetBranchAddress("max_detajj",&max_detajj);
//    sTree->SetBranchAddress("cos_Z_2jets",&cos_Z_2jets);
//    sTree->SetBranchAddress("central_Z_2jets",&central_Z_2jets);
//    sTree->SetBranchAddress("jet_central_ST",&jet_central_ST);
//    sTree->SetBranchAddress("jet30_central_ST",&jet30_central_ST);
//    sTree->SetBranchAddress("jet40_central_ST",&jet40_central_ST);
//    sTree->SetBranchAddress("jet50_central_ST",&jet50_central_ST);
//    sTree->SetBranchAddress("n_central_jets",&n_central_jets);
//    sTree->SetBranchAddress("n_central_jets30",&n_central_jets30);
//    sTree->SetBranchAddress("n_central_jets40",&n_central_jets40);
//    sTree->SetBranchAddress("n_central_jets50",&n_central_jets50);
//    sTree->SetBranchAddress("m_leading_jet_pt",&m_leading_jet_pt);
//    sTree->SetBranchAddress("m_leading_jet_eta",&m_leading_jet_eta);
//    sTree->SetBranchAddress("m_leading_jet_rapidity",&m_leading_jet_rapidity);
//    sTree->SetBranchAddress("m_second_jet_pt",&m_second_jet_pt);
//    sTree->SetBranchAddress("m_second_jet_eta",&m_second_jet_eta);
//    sTree->SetBranchAddress("m_second_jet_rapidity",&m_second_jet_rapidity);
//    sTree->SetBranchAddress("m_n_central_jets",&m_n_central_jets);
//    sTree->SetBranchAddress("m_leading_twojets_vsum_pt",&m_leading_twojets_vsum_pt);
//    sTree->SetBranchAddress("dPhiMET_MLeadJet",&dPhiMET_MLeadJet);
//    sTree->SetBranchAddress("dPhiMET_MSecJet",&dPhiMET_MSecJet);
//    sTree->SetBranchAddress("jets_pt",&jets_pt);
//    sTree->SetBranchAddress("jets_eta",&jets_eta);
//    sTree->SetBranchAddress("jets_phi",&jets_phi);
//    sTree->SetBranchAddress("jets_m",&jets_m);
    sTree->SetBranchAddress("met_tst",&met_tst);
    sTree->SetBranchAddress("met_px_tst",&met_px_tst);
    sTree->SetBranchAddress("met_py_tst",&met_py_tst);
//    sTree->SetBranchAddress("met_tst_loose",&met_tst_loose);
//    sTree->SetBranchAddress("met_px_tst_loose",&met_px_tst_loose);
//    sTree->SetBranchAddress("met_py_tst_loose",&met_py_tst_loose);
    sTree->SetBranchAddress("met_soft",&met_soft);
    sTree->SetBranchAddress("met_px_soft",&met_px_soft);
    sTree->SetBranchAddress("met_py_soft",&met_py_soft);
//    sTree->SetBranchAddress("met_jets_pt",&met_jets_pt);
//    sTree->SetBranchAddress("met_muon_pt",&met_muon_pt);
//    sTree->SetBranchAddress("met_electron_pt",&met_electron_pt);
//    sTree->SetBranchAddress("dphi_met_jets",&dphi_met_jets);
    sTree->SetBranchAddress("met_signif",&met_signif);
    sTree->SetBranchAddress("mT_ZZ",&mT_ZZ);
    sTree->SetBranchAddress("Z_eta",&Z_eta);
    sTree->SetBranchAddress("Z_pT",&Z_pT);
//    sTree->SetBranchAddress("Z_rapidity",&Z_rapidity);
//    sTree->SetBranchAddress("met_obj",&met_obj);
    sTree->SetBranchAddress("mZZ_Truth",&mZZ_Truth);
    sTree->SetBranchAddress("rho_Truth",&rho_Truth);
    sTree->SetBranchAddress("ZpTomT",&ZpTomT);
    sTree->SetBranchAddress("M2Lep",&M2Lep);
    sTree->SetBranchAddress("dLepR",&dLepR);
    sTree->SetBranchAddress("dMetZPhi",&dMetZPhi);
    sTree->SetBranchAddress("frac_pT",&frac_pT);
    sTree->SetBranchAddress("dPhiJ25met",&dPhiJ25met);
    sTree->SetBranchAddress("dPhiJ100met",&dPhiJ100met);
    sTree->SetBranchAddress("mT_Hinv",&mT_Hinv);
    sTree->SetBranchAddress("ZpTomT_Hinv",&ZpTomT_Hinv);
    sTree->SetBranchAddress("MetOHT",&MetOHT);
    sTree->SetBranchAddress("sumpT_scalar",&sumpT_scalar);

}

double VBFllvvHM::GetNormSF(int run,  TFile *InputFile,string FileName) {

    double NormSF;
    int yeard = -1;
    if ( FileName.find("r9364") != string::npos ) {
//        lum =36.207660;
        lum =3.219+32.988;
        yeard =16;
    } else if ( FileName.find("r10201") != string::npos ) {
//        lum =44.3074;
        lum =44.307;
        yeard = 17;
    } else if ( FileName.find("r10724") != string::npos ) {
//        lum =59.9372;
        lum =58.450;
        yeard = 18;
    } else {
        cout<<"MCSample Not 16a 16d or 16e"<<endl;
    }

    if (run < 0) {
        cout<<"File not exit"<<endl;
    }
    if ((run > 341367)&& (run < 341386)) {
        NormSF = getxsecVBFHllvv(yeard,run)*lum;
    } else {
        TH1D* hInfo = (TH1D*)InputFile->Get("Hist/hInfo_PFlow");
        double xSec        = hInfo->GetBinContent(1)*2.0/hInfo->GetEntries();
        double SumOfWeight = hInfo->GetBinContent(2);

        NormSF = xSec/SumOfWeight*lum;
    }
    return NormSF;

}

double VBFllvvHM::GetNormSF(int run, string FileName,string OutFileName) {
    double NormSF;
    int yeard = -1;
    if ( FileName.find("r9364") != string::npos ) {
//        lum =36.207660;
        lum =3.219+32.988;
        yeard =16;
    } else if ( FileName.find("r10201") != string::npos ) {
//        lum =44.3074;
        lum =44.307;
        yeard = 17;
    } else if ( FileName.find("r10724") != string::npos ) {
//        lum =59.9372;
        lum =58.450;
        yeard = 18;
    } else {
        cout<<"MCSample Not 16a 16d or 16e"<<endl;
    }

//    cout<< yeard<<endl;
//    cout<< OutFileName<<endl;
//    cout<< FileName<<endl;

    if ((run == 364254)||(run ==364285)) {
        if (OutFileName.find("WW")!= string::npos) {
            if ( FileName.find("isZZWW0") != string::npos ) {
                NormSF = getxsecVBFHllvv(yeard,run, 0) * lum ;
            } else if ( FileName.find("isZZWW1") != string::npos ) NormSF = getxsecVBFHllvv(yeard,run, 1) * lum ;
            else if ( FileName.find("isZZWW-1") != string::npos ) NormSF = getxsecVBFHllvv(yeard,run, -1) * lum ;
        } else if (OutPutName.find("ZZ")!= string::npos) {
            NormSF = getxsecVBFHllvv(yeard,run, 2) * lum ;
        }

    } else {
        NormSF = getxsecVBFHllvv(yeard,run)*lum;
    }
    return NormSF;
}

void VBFllvvHM::Loop3lCR(TTree* sTree,string inName ) {
//	cout << inName<<endl;
    for(int ii=0; ii<sTree->GetEntries(); ii++) {
        sTree->GetEntry(ii);
        double weightF;
        string Type = "";
        bool doCrackCut =false;

        if ( (inName.find("r9364") != string::npos) ||(inName.find("data15") != string::npos)||(inName.find("data16") != string::npos  )) {
            doCrackCut = true;
        }

        if(isMC) {
            FileNormSF = GetNormSF(run,inName,OutPutName) *0.83 ;
            if (ii == 0) {
                cout << FileNormSF <<endl;
            }
            weightF = weight * FileNormSF;
        } else weightF =1;
        TLorentzVector MISET(1,1,1,1);
        TLorentzVector lep3rdSET(1,1,1,1);
        MISET.SetPxPyPzE(met_px_tst,met_py_tst,0,sqrt(met_px_tst*met_px_tst+met_py_tst*met_py_tst));
        lep3rdSET.SetPtEtaPhiM(lep3rd_pt,lep3rd_eta,lep3rd_phi,lep3rd_m);
        double MWt =0;
        MCMH3lCRTotal["eee"]->CutFlow2->Fill(0.0,weightF);
        if( !((event_type ==0) || (event_type ==1)) ) {
            continue;
        }
        MCMH3lCRTotal["eee"]->CutFlow2->Fill(1.0,weightF);

        if( event_3CR ==0  ) {
            continue;
        }
        MCMH3lCRTotal["eee"]->CutFlow2->Fill(2.0,weightF);

        if (event_3CR ==1) Type = "mumumu";
        else if (event_3CR ==2) Type = "mumue";
        else if (event_3CR ==3) Type = "eee";
        else if (event_3CR ==4) Type = "eemu";

        if (doCrackCut) {
            if (event_3CR ==2) {
                if( fabs(lep3rd_eta)>1.37 &&  fabs(lep3rd_eta) <1.52) continue;
            } else if (event_3CR ==3) {
                if( (fabs(lep3rd_eta)>1.37 && fabs(lep3rd_eta) <1.52) || (fabs(lepplus_eta)>1.37 && fabs(lepplus_eta) <1.52) || (fabs(lepminus_eta)>1.37 && fabs(lepminus_eta) <1.52) ) {
                    continue;
                }

            } else if (event_3CR ==4) {
                if( (fabs(lepplus_eta)>1.37 && fabs(lepplus_eta) <1.52) || (fabs(lepminus_eta)>1.37 && fabs(lepminus_eta) <1.52) )  {
                    continue;
                }
            }

        }


        MWt = sqrt(2*lep3rdSET.Pt()*MISET.Pt()*(1 - cos(CaldPhi(lep3rdSET.Phi(),MISET.Phi()))));
        MCMH3lCRTotal[Type]->CutFlow1->Fill(0.0,weightF);
        for (auto& tt : MCMH3lCR ) {
            if (tt.first == Type) {
                MCMH3lCRTotal[Type]->CutFlow1->Fill(1.0,weightF);
//		if((medium_3rd == 0) && (lep3rd_pt > 20)) cout << medium_3rd <<" " <<lep3rd_pt<<endl;
//                if((medium_3rd == 1) && (lep3rd_pt < 20)) cout << medium_3rd <<" " <<lep3rd_pt<<endl;
                if( (M2Lep > 106) || (M2Lep < 76) ) cout<<M2Lep<<endl;
                if( (lep3rd_pt) < 20 ) continue;
                MCMH3lCRTotal[Type]->CutFlow1->Fill(2.0,weightF);
                if( (medium_3rd) != 1 ) continue;
                MCMH3lCRTotal[Type]->CutFlow1->Fill(3.0,weightF);
                if( MWt < 60 ) continue;
                MCMH3lCRTotal[Type]->CutFlow1->Fill(4.0,weightF);
                if( n_bjets != 0 ) continue;
                MCMH3lCRTotal[Type]->CutFlow1->Fill(5.0,weightF);
//		if( met_signif < 3 ) continue;

                if (n_jets <2 ) continue;
                MCMH3lCRTotal[Type]->CutFlow1->Fill(6.0,weightF);
                if ((leading_jet_pt < 30)||(second_jet_pt < 30)) continue;
                MCMH3lCRTotal[Type]->CutFlow1->Fill(7.0,weightF);

                MCMH3lCR[Type]->THList[0]->Fill(met_tst,weight);
                MCMH3lCR[Type]->THList[7]->Fill(met_signif,weight);
                MCMH3lCR[Type]->THList[14]->Fill(lep3rd_pt,weight);
                MCMH3lCR[Type]->THList[16]->Fill(lep3rd_phi,weight);
                MCMH3lCR[Type]->THList[15]->Fill(lep3rd_eta,weight);
                MCMH3lCR[Type]->THList[13]->Fill(MWt,weight);
                MCMH3lCR[Type]->THList[4]->Fill(n_bjets,weight);
                MCMH3lCR[Type]->THList[8]->Fill(n_jets,weight);
                MCMH3lCR[Type]->THList[9]->Fill(leading_jet_pt,weight);
                MCMH3lCR[Type]->THList[10]->Fill(second_jet_pt,weight);
                MCMH3lCR[Type]->THList[11]->Fill(mjj,weight);
                MCMH3lCR[Type]->THList[12]->Fill(detajj,weight);
                MCMH3lCR[Type]->THList[17]->Fill(M2Lep,weight);
            }
        }
    }
}

void VBFllvvHM::LoopEMuTau(TTree* sTree, TFile *InputFile, string inName) {

    for(int ii=0; ii<sTree->GetEntries(); ii++) {
        sTree->GetEntry(ii);
        double weightF;
        string Type = "";
        bool OldCut =0;
        if(isMC) {
            FileNormSF = GetNormSF(run,InputFile,inName) ;
            if (ii == 0)
                cout << FileNormSF <<endl;
            weightF = weight * FileNormSF;
        } else weightF =1;
//        if (event_type > 1 ) continue;

        if (event_type == 0) {
            Type ="mumu";
        } else if(event_type == 1) {
            Type = "ee";
        }

        for (auto& tt : VBFP ) {
            if (tt.first == Type) {
                VBFPTotal[Type]->CutFlow1->Fill(0.0,weightF);
                if (event_3CR !=0) continue;
                VBFPTotal[Type]->CutFlow1->Fill(1.0,weightF);
//                VBFP[Type]->THList[0]->Fill(met_tst,weight);
                if (met_tst < 120 ) continue;
                VBFPTotal[Type]->CutFlow1->Fill(2.0,weightF);

//                VBFP[Type]->THList[1]->Fill(dLepR,weight);
                if (fabs(dLepR)>1.8) continue;
                VBFPTotal[Type]->CutFlow1->Fill(3.0,weightF);

//                VBFP[Type]->THList[2]->Fill(dMetZPhi,weight);
                if (fabs(dMetZPhi) < 2.5 ) continue;
                VBFPTotal[Type]->CutFlow1->Fill(4.0,weightF);

//                VBFP[Type]->THList[3]->Fill(dPhiJ100met,weight);
                if (fabs(dPhiJ100met) < 0.4 ) continue;
                VBFPTotal[Type]->CutFlow1->Fill(5.0,weightF);

//                VBFP[Type]->THList[4]->Fill(n_bjets,weight);
                if (n_bjets != 0 ) continue;
                VBFPTotal[Type]->CutFlow1->Fill(6.0,weightF);

                if(OldCut) {
                    if (frac_pT  > 0.2) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(7.0,weightF);
                    if (MetOHT < 0.4 ) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(8.0,weightF);
                    VBFPTotal[Type]->CutFlow1->Fill(9.0,weightF);
                } else {
//                VBFP[Type]->THList[5]->Fill(frac_pT,weight);
                    if (frac_pT < 0.2) {
                        VBFPTotal[Type]->CutFlow1->Fill(7.0,weightF);

//                    VBFP[Type]->THList[6]->Fill(MetOHT,weight);
                        if (MetOHT >0.4 ) {
                            VBFPTotal[Type]->CutFlow1->Fill(8.0,weightF);
                        }
                    }
//                VBFP[Type]->THList[7]->Fill(met_signif,weight);
                    if (met_signif < 10) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(9.0,weightF);
                }

                if (doVBFSel) {
                    if (n_jets <2 ) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(10.0,weightF);

                    if ((leading_jet_pt < 30)||(second_jet_pt<30)) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(11.0,weightF);

                    if (mjj < 550) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(12.0,weightF);

                    if (fabs(detajj) < 4.4) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(13.0,weightF);

                    VBFP[Type]->THList[0]->Fill(met_tst,weight);
                    VBFP[Type]->THList[1]->Fill(dLepR,weight);
                    VBFP[Type]->THList[2]->Fill(dMetZPhi,weight);
                    VBFP[Type]->THList[3]->Fill(dPhiJ100met,weight);
                    VBFP[Type]->THList[4]->Fill(n_bjets,weight);
                    VBFP[Type]->THList[5]->Fill(frac_pT,weight);
                    VBFP[Type]->THList[6]->Fill(MetOHT,weight);
                    VBFP[Type]->THList[7]->Fill(met_signif,weight);
                    VBFP[Type]->THList[8]->Fill(n_jets,weight);
                    VBFP[Type]->THList[9]->Fill(leading_jet_pt,weight);
                    VBFP[Type]->THList[10]->Fill(second_jet_pt,weight);
                    VBFP[Type]->THList[11]->Fill(mjj,weight);
                    VBFP[Type]->THList[12]->Fill(detajj,weight);
                    VBFP[Type]->THList[18]->Fill(mT_ZZ,weight);
                }
            }
        }
    }
}




void VBFllvvHM::LoopEMuTau(TTree* sTree,string inName ) {

    for(int ii=0; ii<sTree->GetEntries(); ii++) {
        sTree->GetEntry(ii);
        double weightF;
        string Type = "";
        bool OldCut =0;
        if(isMC) {
            FileNormSF = GetNormSF(run,inName,OutPutName) ;
            if (ii == 0)
                cout << FileNormSF <<endl;
            weightF = weight * FileNormSF;
        } else weightF =1;
//        if (event_type > 1 ) continue;

        if (event_type == 0) {
            Type ="mumu";
        } else if(event_type == 1) {
            Type = "ee";
        }

        for (auto& tt : VBFP ) {
            if (tt.first == Type) {
                VBFPTotal[Type]->CutFlow1->Fill(0.0,weightF);
                if (event_3CR !=0) continue;
                VBFPTotal[Type]->CutFlow1->Fill(1.0,weightF);
//                VBFP[Type]->THList[0]->Fill(met_tst,weight);
                if (met_tst < 120 ) continue;
                VBFPTotal[Type]->CutFlow1->Fill(2.0,weightF);

//                VBFP[Type]->THList[1]->Fill(dLepR,weight);
                if (fabs(dLepR)>1.8) continue;
                VBFPTotal[Type]->CutFlow1->Fill(3.0,weightF);

//                VBFP[Type]->THList[2]->Fill(dMetZPhi,weight);
                if (fabs(dMetZPhi) < 2.5 ) continue;
                VBFPTotal[Type]->CutFlow1->Fill(4.0,weightF);

//                VBFP[Type]->THList[3]->Fill(dPhiJ100met,weight);
                if (fabs(dPhiJ100met) < 0.4 ) continue;
                VBFPTotal[Type]->CutFlow1->Fill(5.0,weightF);

//                VBFP[Type]->THList[4]->Fill(n_bjets,weight);
                if (n_bjets != 0 ) continue;
                VBFPTotal[Type]->CutFlow1->Fill(6.0,weightF);

                if(OldCut) {
                    if (frac_pT  > 0.2) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(7.0,weightF);
                    if (MetOHT < 0.4 ) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(8.0,weightF);
                    VBFPTotal[Type]->CutFlow1->Fill(9.0,weightF);
                } else {
//                VBFP[Type]->THList[5]->Fill(frac_pT,weight);
                    if (frac_pT < 0.2) {
                        VBFPTotal[Type]->CutFlow1->Fill(7.0,weightF);

//                    VBFP[Type]->THList[6]->Fill(MetOHT,weight);
                        if (MetOHT >0.4 ) {
                            VBFPTotal[Type]->CutFlow1->Fill(8.0,weightF);
                        }
                    }
//                VBFP[Type]->THList[7]->Fill(met_signif,weight);
                    if (met_signif < 10) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(9.0,weightF);
                }

                if (doVBFSel) {
                    if (n_jets <2 ) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(10.0,weightF);

                    if ((leading_jet_pt < 30)||(second_jet_pt<30)) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(11.0,weightF);

                    if (mjj < 550) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(12.0,weightF);

                    if (fabs(detajj) < 4.4) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(13.0,weightF);

                    VBFP[Type]->THList[0]->Fill(met_tst,weight);
                    VBFP[Type]->THList[1]->Fill(dLepR,weight);
                    VBFP[Type]->THList[2]->Fill(dMetZPhi,weight);
                    VBFP[Type]->THList[3]->Fill(dPhiJ100met,weight);
                    VBFP[Type]->THList[4]->Fill(n_bjets,weight);
                    VBFP[Type]->THList[5]->Fill(frac_pT,weight);
                    VBFP[Type]->THList[6]->Fill(MetOHT,weight);
                    VBFP[Type]->THList[7]->Fill(met_signif,weight);
                    VBFP[Type]->THList[8]->Fill(n_jets,weight);
                    VBFP[Type]->THList[9]->Fill(leading_jet_pt,weight);
                    VBFP[Type]->THList[10]->Fill(second_jet_pt,weight);
                    VBFP[Type]->THList[11]->Fill(mjj,weight);
                    VBFP[Type]->THList[12]->Fill(detajj,weight);
                    VBFP[Type]->THList[18]->Fill(mT_ZZ,weight);
                }
            }
        }
    }
}

void VBFllvvHM::NormPlot( MakeHist* InClass, double NSF) {

    for (auto vec = (InClass->THList).begin(); vec < (InClass->THList).end(); vec++) {
        (*vec)->Scale((NSF));
    }

//    InClass->MWT->Scale(NSF);
//    InClass->lep3Pt->Scale(NSF);
//    InClass->lep3Phi->Scale(NSF);
//    InClass->lep3Eta->Scale(NSF);

}

void VBFllvvHM::AddPlot(MakeHist*Base,MakeHist*Indep) {

    for (size_t ii = 0; ii < ((Base->THList).size()); ii++) {
        Base->THList[ii]->Add(Indep->THList[ii]);
    }

//    Base->MWT->Add(Indep->MWT);
//    Base->lep3Pt->Add(Indep->lep3Pt);
//    Base->lep3Phi->Add(Indep->lep3Phi);
//    Base->lep3Eta->Add(Indep->lep3Eta);


}


void VBFllvvHM::WritePlot(MakeHist*Base, TFile*OutFile) {

    for (auto vec = (Base->THList).begin(); vec < (Base->THList).end(); vec++) {
        (*vec)->Write();
    }

//    Base->MWT->Write();
//    Base->lep3Pt->Write();
//    Base->lep3Phi->Write();
//    Base->lep3Eta->Write();


}

void VBFllvvHM::WriteCutFlow(MakeHist*Base, TFile*OutFile) {
    Base->CutFlow1->Write();
//    if (doVBFSel)
//        Base->CutFlow2->Write();

}

void VBFllvvHM::RunProcessor3lCR() {
    bool readcutflow = 0;
    TFile* outputfile = new TFile((OutPutName+"Plots.root").c_str(),"recreate");
    typedef std::vector<std::string>::iterator STRING_ITOR;
    STRING_ITOR inF;
    FileNormSF = 0;
//    MakeHist * SumT = new MakeHist("VBF3lCRTAllCh",OutPutName);
//    SumT->MKCutFlow();

    for (auto& x : MCMH3lCRTotal ) {
        x.second = new MakeHist("VBF3lCRT"+x.first,OutPutName);
        x.second->MKTH1D();
        x.second->MKCutFlow();
    }

    int Itag=0;
    for (inF = InputFileList.begin(); inF != InputFileList.end(); ++inF) {
        Itag++;
        TFile* testF = new TFile((*inF).c_str(),"READ");
        TTree* SkimTree = (TTree*)testF->Get("tree_NOMINAL");

        for (auto& xx : MCMH3lCR ) {
            xx.second = new MakeHist("VBF3lCR"+xx.first+convertDouble(Itag),OutPutName);
            xx.second->MKTH1D();
        }

        cout<<"loop file:"<<(*inF).c_str()<<endl;
        ReadTreeSkimed(SkimTree);
        Loop3lCR(SkimTree, (*inF));
        if(isMC) {
            for (auto& yy : MCMH3lCR ) {
                NormPlot(yy.second, FileNormSF);
            }
        }
        for (auto& zz : MCMH3lCR ) {
            AddPlot(MCMH3lCRTotal[zz.first],zz.second );
            delete zz.second;
        }
        delete testF;

    }

    for (auto& zt : MCMH3lCR ) {
        AddPlot(MCMH3lCRTotal["AllCh"],MCMH3lCRTotal[zt.first]);
        MCMH3lCRTotal["AllCh"]->CutFlow1->Add(MCMH3lCRTotal[zt.first]->CutFlow1);
    }
//	SumT->CutFlow1->Add(MCMH3lCRTotal["mumumu"]->CutFlow1);
//	SumT->CutFlow1->Add(MCMH3lCRTotal["mumue"]->CutFlow1);
//	SumT->CutFlow1->Add(MCMH3lCRTotal["eee"]->CutFlow1);
//	SumT->CutFlow1->Add(MCMH3lCRTotal["eemu"]->CutFlow1);

    if(readcutflow) {
//        MCMH3lCRTotal["eee"]->ReadCutFlow(MCMH3lCRTotal["eee"]->CutFlow2);
        MCMH3lCRTotal["mumumu"]->ReadCutFlow(MCMH3lCRTotal["mumumu"]->CutFlow1);
        MCMH3lCRTotal["mumue"]->ReadCutFlow(MCMH3lCRTotal["mumue"]->CutFlow1);
        MCMH3lCRTotal["eee"]->ReadCutFlow(MCMH3lCRTotal["eee"]->CutFlow1);
        MCMH3lCRTotal["eemu"]->ReadCutFlow(MCMH3lCRTotal["eemu"]->CutFlow1);
//        SumT->ReadCutFlow(SumT->CutFlow1);
    }
    outputfile->cd();
    for (auto& xn : MCMH3lCRTotal ) {
        WritePlot(xn.second,outputfile);
        WriteCutFlow(xn.second,outputfile);
    }

//    SumT->CutFlow1->Write();
    outputfile->Close();


}


void VBFllvvHM::RunProcessor() {

    bool readcutflow = 0;
    TFile* outputfile = new TFile((OutPutName+"Plots.root").c_str(),"recreate");
    typedef std::vector<std::string>::iterator STRING_ITOR;
    STRING_ITOR inF;
    FileNormSF = 0;
    for (auto& x : VBFPTotal ) {
        x.second = new MakeHist("VBFHMllvvT"+x.first,OutPutName);
        x.second->MKTH1D();
        x.second->MKCutFlow();
    }
    int Itag=0;
    for (inF = InputFileList.begin(); inF != InputFileList.end(); ++inF) {
        Itag++;
        TFile* testF = new TFile((*inF).c_str(),"READ");
//        TTree* SkimTree = (TTree*)testF->Get("tree_NOMINAL");
        TTree* SkimTree = (TTree*)testF->Get("tree_PFLOW");

        for (auto& xx : VBFP ) {
            xx.second = new MakeHist("VBFHMllvv"+xx.first+convertDouble(Itag),OutPutName);
            xx.second->MKTH1D();
        }
        cout<<"loop file:"<<(*inF).c_str()<<endl;
        ReadTreeSkimed(SkimTree);
//        LoopEMuTau(SkimTree, (*inF));
        LoopEMuTau(SkimTree, testF,(*inF));
        if(isMC) {
            for (auto& yy : VBFP ) {
                NormPlot(yy.second, FileNormSF);
            }

        }

        for (auto& zz : VBFP ) {
            AddPlot(VBFPTotal[zz.first],zz.second );
            delete zz.second;
        }

        delete testF;
    }

    for (auto& zt : VBFPTotal ) {
        AddPlot(VBFPTotal["AllCh"],VBFPTotal[zt.first]);
        VBFPTotal["AllCh"]->CutFlow1->Add(VBFPTotal[zt.first]->CutFlow1);
    }

    if(readcutflow) {
        cout<<"ee events " <<endl;
        VBFPTotal["ee"]->ReadCutFlow(VBFPTotal["ee"]->CutFlow1);
        cout<<" " <<endl;
        cout<<"mumu events " <<endl;
        VBFPTotal["mumu"]->ReadCutFlow(VBFPTotal["mumu"]->CutFlow1);

    }
    outputfile->cd();

    for (auto& xn : VBFPTotal ) {
        WritePlot(xn.second,outputfile);
        WriteCutFlow(xn.second,outputfile);
    }

    outputfile->Close();
}


void VBFllvvHM::RunProcessor(string treename) {

    bool readcutflow = 0;
    TFile* outputfile = new TFile((OutPutName+"Plots.root").c_str(),"recreate");
    typedef std::vector<std::string>::iterator STRING_ITOR;
    STRING_ITOR inF;
    FileNormSF = 0;
    for (auto& x : VBFPTotal ) {
        x.second = new MakeHist("VBFHMllvvT"+x.first,OutPutName);
        x.second->MKTH1D();
        x.second->MKCutFlow();
    }
    int Itag=0;
    for (inF = InputFileList.begin(); inF != InputFileList.end(); ++inF) {
        Itag++;
        TFile* testF = new TFile((*inF).c_str(),"READ");
//        TTree* SkimTree = (TTree*)testF->Get("tree_NOMINAL");
        TTree* SkimTree = (TTree*)testF->Get(treename.c_str());

        for (auto& xx : VBFP ) {
            xx.second = new MakeHist("VBFHMllvv"+xx.first+convertDouble(Itag),OutPutName);
            xx.second->MKTH1D();
        }
        cout<<"loop file:"<<(*inF).c_str()<<endl;
        ReadTreeSkimed(SkimTree);
//        LoopEMuTau(SkimTree, (*inF));
        LoopEMuTau(SkimTree, testF,(*inF));
        if(isMC) {
            for (auto& yy : VBFP ) {
                NormPlot(yy.second, FileNormSF);
            }

        }

        for (auto& zz : VBFP ) {
            AddPlot(VBFPTotal[zz.first],zz.second );
            delete zz.second;
        }

        delete testF;
    }

    for (auto& zt : VBFPTotal ) {
        AddPlot(VBFPTotal["AllCh"],VBFPTotal[zt.first]);
        VBFPTotal["AllCh"]->CutFlow1->Add(VBFPTotal[zt.first]->CutFlow1);
    }

    if(readcutflow) {
        cout<<"ee events " <<endl;
        VBFPTotal["ee"]->ReadCutFlow(VBFPTotal["ee"]->CutFlow1);
        cout<<" " <<endl;
        cout<<"mumu events " <<endl;
        VBFPTotal["mumu"]->ReadCutFlow(VBFPTotal["mumu"]->CutFlow1);

    }
    outputfile->cd();

    for (auto& xn : VBFPTotal ) {
        WritePlot(xn.second,outputfile);
        WriteCutFlow(xn.second,outputfile);
    }

    outputfile->Close();
}
