#include "getxsec.h"



double getxsecVBFHllvv( int year, double ChannelNumber, int WWtype){
double NormSF = -1;
if (year == 16)
{
     if ( ChannelNumber ==   341350 ){ NormSF =   0.000200492;}
else if ( ChannelNumber ==   341351 ){ NormSF =   0.000402184;}
else if ( ChannelNumber ==   341352 ){ NormSF =   0.000449263;}
else if ( ChannelNumber ==   341353 ){ NormSF =   0.000150917;}
else if ( ChannelNumber ==   341354 ){ NormSF =   0.00078365 ;}
else if ( ChannelNumber ==   341355 ){ NormSF =   0.001029544;}
else if ( ChannelNumber ==   341356 ){ NormSF =   0.001327276;}
else if ( ChannelNumber ==   341357 ){ NormSF =   0.000455863;}
else if ( ChannelNumber ==   341358 ){ NormSF =   0.002832807;}
else if ( ChannelNumber ==   341359 ){ NormSF =   0.00419363;}  
else if ( ChannelNumber ==   341360 ){ NormSF =   0.005930086;}
else if ( ChannelNumber ==   341361 ){ NormSF =   0.00809921;}
else if ( ChannelNumber ==   341362 ){ NormSF =   0.010959098;}
else if ( ChannelNumber ==   341364 ){ NormSF =   0.089527251;}
else if ( ChannelNumber ==   341367 ){ NormSF =   0.032693403;}
else if ( ChannelNumber ==   341368 ){ NormSF =  0.000415695;}
else if ( ChannelNumber ==   341369 ){ NormSF =  0.000866452;}
else if ( ChannelNumber ==   341370 ){ NormSF =  0.000923863;}
else if ( ChannelNumber ==   341371 ){ NormSF =  0.000416577;}
else if ( ChannelNumber ==   341372 ){ NormSF =  0.000830912;}
else if ( ChannelNumber ==   341373 ){ NormSF =  0.000913759;}
else if ( ChannelNumber ==   341374 ){ NormSF =  0.000871634;}
else if ( ChannelNumber ==   341375 ){ NormSF =  0.000412682;}
else if ( ChannelNumber ==   341376 ){ NormSF =  0.001162117;}
else if ( ChannelNumber ==   341377 ){ NormSF =  0.00091764 ;}
else if ( ChannelNumber ==   341378 ){ NormSF =  0.000911681;}
else if ( ChannelNumber ==   341379 ){ NormSF =  0.000929082;}
else if ( ChannelNumber ==   341380 ){ NormSF =  0.000837709;}
else if ( ChannelNumber ==   341382 ){ NormSF =  0.001022957;}
else if ( ChannelNumber ==   341385 ){ NormSF =  0.000907566;}
//if ( ChannelNumber == 341350 ){ NormSF =    0.00002596838291;}
//else if ( ChannelNumber == 341353 ){ NormSF =    0.000009136422703;}
//else if ( ChannelNumber == 341357 ){ NormSF =    0.0001633694649;}
//else if ( ChannelNumber == 341368 ){ NormSF =    0.004452707248;}
//else if ( ChannelNumber == 341369 ){ NormSF =    0.009147290669;}
//else if ( ChannelNumber == 341370 ){ NormSF =    0.009591599116;}
//else if ( ChannelNumber == 341371 ){ NormSF =    0.004407386356;}
//else if ( ChannelNumber == 341372 ){ NormSF =    0.0088797444;}
//else if ( ChannelNumber == 341373 ){ NormSF =    0.009461247587;}
//else if ( ChannelNumber == 341374 ){ NormSF =    0.00930718634;}
//else if ( ChannelNumber == 341375 ){ NormSF =    0.004318288094;}
//else if ( ChannelNumber == 341376 ){ NormSF =    0.01205915507;}
//else if ( ChannelNumber == 341377 ){ NormSF =    0.009492186486;}
//else if ( ChannelNumber == 341378 ){ NormSF =    0.009570394686;}
//else if ( ChannelNumber == 341379 ){ NormSF =    0.01038784644;}
//else if ( ChannelNumber == 341380 ){ NormSF =    0.008787977401;}
//else if ( ChannelNumber == 341382 ){ NormSF =    0.0103282072;}
//else if ( ChannelNumber == 341385 ){ NormSF =    0.009684938695;}
else if ( ChannelNumber == 341368 ){ NormSF =    0.004452707248;}
else if ( ChannelNumber == 341371 ){ NormSF =    0.004407386366;}
else if ( ChannelNumber == 341375 ){ NormSF =    0.004318288062;}
else if ( ChannelNumber == 345706 ){ NormSF =    0.000008640931551;}
else if ( ChannelNumber == 345723 ){ NormSF =    0.00007098423343;}
else if ( ChannelNumber == 343233 ){ NormSF =    0.000045806;}
else if ( ChannelNumber == 345666 ){ NormSF =    0.001376428998;}
else if ( ChannelNumber == 361603 ){ NormSF =    0.0002510604097;}
else if ( ChannelNumber == 361605 ){ NormSF =    0.004965998315;}
else if ( ChannelNumber == 361604 ){ NormSF =    0.001010518342;}
else if ( ChannelNumber == 364283 ){ NormSF =    0.00003030493006;}
else if ( ChannelNumber == 361601 ){ NormSF =    0.0001000811235;}
else if ( ChannelNumber == 361602 ){ NormSF =    0.001015334419;}
else if ( ChannelNumber == 361607 ){ NormSF =    0.0006732921165;}
else if ( ChannelNumber == 361609 ){ NormSF =    0.0001024532146;}
else if ( ChannelNumber == 364253 ){ NormSF =    0.0008543912306;}
else if ( ChannelNumber == 364284 ){ NormSF =    0.0004166749081;}
else if ( ChannelNumber == 364254 ){
       if( WWtype == 0)	NormSF =    0.002443887655;
       else if ( WWtype == -1) NormSF = 0.002443887655;
       else if ( WWtype == 1) NormSF = -0.001221943828;
       else if ( WWtype == 2) NormSF = 0.002443887655*1.5;
       if ( WWtype == -99) std::cout<<"Warning: NormSF got Wrong"<<std::endl;
}
else if ( ChannelNumber == 345718 ){ NormSF =    0.00001419936646;}
else if ( ChannelNumber == 364285 ){ 
	if ( WWtype == 0) NormSF =    0.0003844552902;
	else if ( WWtype == -1) NormSF = 0.0003844552902;
        else if ( WWtype == 1) NormSF = -0.0001922276451;
        else if ( WWtype == 2) NormSF = 0.0003844552902*1.5;
       if ( WWtype == -99) std::cout<<"Warning: NormSF got Wrong"<<std::endl;
}
//else if ( ChannelNumber == 364285 ){ NormSF =    0.0003844552902;}
//else if ( ChannelNumber == 364285 ){ NormSF =    -0.0001922276451;}
else if ( ChannelNumber == 410472 ){ NormSF =    0.000001504782008;}
else if ( ChannelNumber == 410644 ){ NormSF =    0.0005082030328;}
else if ( ChannelNumber == 410645 ){ NormSF =    0.0005077161566;}
else if ( ChannelNumber == 410658 ){ NormSF =    0.0002489397595;}
else if ( ChannelNumber == 410659 ){ NormSF =    0.0002486114349;}
else if ( ChannelNumber == 410648 ){ NormSF =    0.0009446140719;}
else if ( ChannelNumber == 410649 ){ NormSF =    0.0009452160764;}
else if ( ChannelNumber == 410646 ){ NormSF =    0.0000199191783;}
else if ( ChannelNumber == 410647 ){ NormSF =    0.00001992232919;}
else if ( ChannelNumber == 364128 ){ NormSF =    0.2952759223;}
else if ( ChannelNumber == 364129 ){ NormSF =    0.07378050775;}
else if ( ChannelNumber == 364130 ){ NormSF =    0.03088851631;}
else if ( ChannelNumber == 364131 ){ NormSF =    0.03409796125;}
else if ( ChannelNumber == 364132 ){ NormSF =    0.02803135421;}
else if ( ChannelNumber == 364133 ){ NormSF =    0.005720789094;}
else if ( ChannelNumber == 364134 ){ NormSF =    0.008303802689;}
else if ( ChannelNumber == 364135 ){ NormSF =    0.004648225778;}
else if ( ChannelNumber == 364136 ){ NormSF =    0.001784310396;}
else if ( ChannelNumber == 364137 ){ NormSF =    0.002835009247;}
else if ( ChannelNumber == 364138 ){ NormSF =    0.002443463587;}
else if ( ChannelNumber == 364139 ){ NormSF =    0.0008016778313;}
else if ( ChannelNumber == 364140 ){ NormSF =    0.0005919720452;}
else if ( ChannelNumber == 364141 ){ NormSF =    0.0001419739531;}
else if ( ChannelNumber == 364100 ){ NormSF =    0.2956683942;}
else if ( ChannelNumber == 364101 ){ NormSF =    0.0764352065;}
else if ( ChannelNumber == 364102 ){ NormSF =    0.03084950857;}
else if ( ChannelNumber == 364103 ){ NormSF =    0.03386555589;}
else if ( ChannelNumber == 364104 ){ NormSF =    0.02840792077;}
else if ( ChannelNumber == 364105 ){ NormSF =    0.006005689094;}
else if ( ChannelNumber == 364106 ){ NormSF =    0.007880718163;}
else if ( ChannelNumber == 364107 ){ NormSF =    0.004592915748;}
else if ( ChannelNumber == 364108 ){ NormSF =    0.0007028124315;}
else if ( ChannelNumber == 364109 ){ NormSF =    0.002694594156;}
else if ( ChannelNumber == 364110 ){ NormSF =    0.002410952686;}
else if ( ChannelNumber == 364111 ){ NormSF =    0.0007808926151;}
else if ( ChannelNumber == 364112 ){ NormSF =    0.0005846647435;}
else if ( ChannelNumber == 364113 ){ NormSF =    0.0001415123154;}
else if ( ChannelNumber == 364114 ){ NormSF =    0.2952690043;}
else if ( ChannelNumber == 364115 ){ NormSF =    0.07671226985;}
else if ( ChannelNumber == 364116 ){ NormSF =    0.03092697322;}
else if ( ChannelNumber == 364117 ){ NormSF =    0.03441780363;}
else if ( ChannelNumber == 364118 ){ NormSF =    0.02783829473;}
else if ( ChannelNumber == 364119 ){ NormSF =    0.00617487011;}
else if ( ChannelNumber == 364120 ){ NormSF =    0.008160475743;}
else if ( ChannelNumber == 364121 ){ NormSF =    0.004615581558;}
else if ( ChannelNumber == 364122 ){ NormSF =    0.0007049332136;}
else if ( ChannelNumber == 364123 ){ NormSF =    0.00277920438;}
else if ( ChannelNumber == 364124 ){ NormSF =    0.002450139858;}
else if ( ChannelNumber == 364125 ){ NormSF =    0.0007956755838;}
else if ( ChannelNumber == 364126 ){ NormSF =    0.0005942955485;}
else if ( ChannelNumber == 364127 ){ NormSF =    0.000142688664;}
else if ( ChannelNumber == 364242 ){ NormSF =    0.003154788883;}
else if ( ChannelNumber == 364243 ){ NormSF =    0.002580590558;}
else if ( ChannelNumber == 364244 ){ NormSF =    0.002562508334;}
else if ( ChannelNumber == 364245 ){ NormSF =    0.001023814459;}
else if ( ChannelNumber == 364246 ){ NormSF =    0.0004638353491;}
else if ( ChannelNumber == 364247 ){ NormSF =    0.0003779962458;}
else if ( ChannelNumber == 364248 ){ NormSF =    0.00008258633886;}
else if ( ChannelNumber == 364249 ){ NormSF =    0.0001626819831;}
else if ( ChannelNumber == 410081 ){ NormSF =    0.00019808;}
else if ( ChannelNumber == 410142 ){ NormSF =    0.00006848824613;}
else if ( ChannelNumber == 410155 ){ NormSF =    0.0001462103333;}
else if ( ChannelNumber == 410156 ){ NormSF =    0.0007402190485;}
else if ( ChannelNumber == 410157 ){ NormSF =    0.0003703438757;}
else if ( ChannelNumber == 364250 ){ NormSF =    0.0001665023698;}
else if ( ChannelNumber == 364283 ){ NormSF =    0.00003030493006;}
else if ( ChannelNumber == 345706 ){ NormSF =    0.000008578864445;}
}

else if (year == 17)
{

     if ( ChannelNumber ==   341350 ){ NormSF = 0.000211046 ;}
else if ( ChannelNumber ==   341351 ){ NormSF = 0.000402119 ;}
else if ( ChannelNumber ==   341352 ){ NormSF = 0.000458412 ;}
else if ( ChannelNumber ==   341353 ){ NormSF = 0.000147145 ;}
else if ( ChannelNumber ==   341354 ){ NormSF = 0.000783512 ;}
else if ( ChannelNumber ==   341355 ){ NormSF = 0.001029545 ;}
else if ( ChannelNumber ==   341356 ){ NormSF = 0.001327176 ;}
else if ( ChannelNumber ==   341357 ){ NormSF = 0.000419396 ;}
else if ( ChannelNumber ==   341358 ){ NormSF = 0.002890466 ;}
else if ( ChannelNumber ==   341359 ){ NormSF = 0.004193361 ;} 
else if ( ChannelNumber ==   341360 ){ NormSF = 0.005929689 ;}
else if ( ChannelNumber ==   341361 ){ NormSF = 0.008098701 ;}
else if ( ChannelNumber ==   341362 ){ NormSF = 0.010957944 ;}
else if ( ChannelNumber ==   341364 ){ NormSF = 0.017905156 ;}
else if ( ChannelNumber ==   341367 ){ NormSF = 0.032692053 ;}
else if ( ChannelNumber ==   341368 ){ NormSF = 0.000319706 ;}
else if ( ChannelNumber ==   341369 ){ NormSF = 0.000831448 ;}
else if ( ChannelNumber ==   341370 ){ NormSF = 0.000911885 ;}
else if ( ChannelNumber ==   341371 ){ NormSF = 0.000322914 ;}
else if ( ChannelNumber ==   341372 ){ NormSF = 0.000834718 ;}
else if ( ChannelNumber ==   341373 ){ NormSF = 0.000831706 ;}
else if ( ChannelNumber ==   341374 ){ NormSF = 0.000834018 ;}
else if ( ChannelNumber ==   341375 ){ NormSF = 0.000322871 ;}
else if ( ChannelNumber ==   341376 ){ NormSF = 0.000832782 ;}
else if ( ChannelNumber ==   341377 ){ NormSF = 0.000834735 ;}
else if ( ChannelNumber ==   341378 ){ NormSF = 0.00089204  ;}
else if ( ChannelNumber ==   341379 ){ NormSF = 0.000846075 ;}
else if ( ChannelNumber ==   341380 ){ NormSF = 0.000924301 ;}
else if ( ChannelNumber ==   341382 ){ NormSF = 0.000822012 ;}
else if ( ChannelNumber ==   341385 ){ NormSF = 0.000821329 ;}
//if ( ChannelNumber == 341350) { NormSF = 0.00002733540692;}
//else if ( ChannelNumber == 341353) { NormSF = 0.00000890810911;}
//else if ( ChannelNumber == 341357) { NormSF = 0.0001503005078;}
//else if ( ChannelNumber == 341368) { NormSF = 0.003424522671;}
//else if ( ChannelNumber == 341369) { NormSF = 0.00877774333;}
//else if ( ChannelNumber == 341370) { NormSF = 0.009467241447;}
//else if ( ChannelNumber == 341371) { NormSF = 0.003416429903;}
//else if ( ChannelNumber == 341372) { NormSF = 0.008920422964;}
//else if ( ChannelNumber == 341373) { NormSF = 0.008611652914;}
//else if ( ChannelNumber == 341374) { NormSF = 0.008905531165;}
//else if ( ChannelNumber == 341375) { NormSF = 0.003378506177;}
//else if ( ChannelNumber == 341376) { NormSF = 0.008641684723;}
//else if ( ChannelNumber == 341377) { NormSF = 0.008634599666;}
//else if ( ChannelNumber == 341378) { NormSF = 0.009364216898;}
//else if ( ChannelNumber == 341379) { NormSF = 0.009459772716;}
//else if ( ChannelNumber == 341380) { NormSF = 0.009696364666;}
//else if ( ChannelNumber == 341382) { NormSF = 0.008299382096;}
//else if ( ChannelNumber == 341385) { NormSF = 0.008764672614;}
else if ( ChannelNumber == 341368) { NormSF = 0.003424522671;}
else if ( ChannelNumber == 341371) { NormSF = 0.00341642991;}
else if ( ChannelNumber == 341375) { NormSF = 0.003378506152;}
else if ( ChannelNumber == 345706) { NormSF = 0.00001010916848;}
else if ( ChannelNumber == 345723) { NormSF = 0.00006169102792;}
else if ( ChannelNumber == 343233) { NormSF = 0.000045806;}
else if ( ChannelNumber == 345666) { NormSF = 0.001099188711;}
else if ( ChannelNumber == 361603) { NormSF = 0.0002528573537;}
else if ( ChannelNumber == 361604) { NormSF = 0.001004065912;}
else if ( ChannelNumber == 361605) { NormSF = 0.004968567888;}
else if ( ChannelNumber == 364283) { NormSF = 0.00002468046757;}
else if ( ChannelNumber == 361601) { NormSF = 0.000100030923;}
else if ( ChannelNumber == 361602) { NormSF = 0.001025635613;}
else if ( ChannelNumber == 361607) { NormSF = 0.0006794811744;}
else if ( ChannelNumber == 361609) { NormSF = 0.0001028455471;}
else if ( ChannelNumber == 364253) { NormSF = 0.0004135334177;}
else if ( ChannelNumber == 364284) { NormSF = 0.000331614284;}
else if ( ChannelNumber == 364254) { 
	 if( WWtype == 0) NormSF = 0.001220941113;
       else if ( WWtype == -1) NormSF = 0.001220941113;
       else if ( WWtype == 1) NormSF = -0.0006104705566;
       else if ( WWtype == 2) NormSF = 0.001220941113*1.5;
       if ( WWtype == -99) std::cout<<"Warning: NormSF got Wrong"<<std::endl;}
else if ( ChannelNumber == 345718) { NormSF = 0.00001126934166;}
else if ( ChannelNumber == 364285) {
	         if( WWtype == 0) NormSF = 0.0003037409957;
       else if ( WWtype == -1) NormSF = 0.0003037409957;
       else if ( WWtype == 1) NormSF = 0.0003037409957*-0.5;
       else if ( WWtype == 2) NormSF = 0.0003037409957*1.5;
       if ( WWtype == -99) std::cout<<"Warning: NormSF got Wrong"<<std::endl;}
else if ( ChannelNumber == 410472) { NormSF = 0.00000267671354;}
else if ( ChannelNumber == 410644) { NormSF = 0.0004069528172;}
else if ( ChannelNumber == 410645) { NormSF = 0.0004062539318;}
else if ( ChannelNumber == 410658) { NormSF = 0.0001915803643;}
else if ( ChannelNumber == 410659) { NormSF = 0.0001983441276;}
else if ( ChannelNumber == 410648) { NormSF = 0.0007556077916;}
else if ( ChannelNumber == 410649) { NormSF = 0.0007567372167;}
else if ( ChannelNumber == 410646) { NormSF = 0.00001593978355;}
else if ( ChannelNumber == 410647) { NormSF = 0.00001595928729;}
else if ( ChannelNumber == 364128) { NormSF = 0.2360016819;}
else if ( ChannelNumber == 364129) { NormSF = 0.05992393699;}
else if ( ChannelNumber == 364130) { NormSF = 0.02473806032;}
else if ( ChannelNumber == 364131) { NormSF = 0.02711011211;}
else if ( ChannelNumber == 364132) { NormSF = 0.02270986392;}
else if ( ChannelNumber == 364133) { NormSF = 0.004549613656;}
else if ( ChannelNumber == 364134) { NormSF = 0.006580439;}
else if ( ChannelNumber == 364135) { NormSF = 0.003750425449;}
else if ( ChannelNumber == 364136) { NormSF = 0.001432728502;}
else if ( ChannelNumber == 364137) { NormSF = 0.002208340823;}
else if ( ChannelNumber == 364138) { NormSF = 0.001978858506;}
else if ( ChannelNumber == 364139) { NormSF = 0.0006366710317;}
else if ( ChannelNumber == 364140) { NormSF = 0.0004735600671;}
else if ( ChannelNumber == 364141) { NormSF = 0.00011357371;}
else if ( ChannelNumber == 364100) { NormSF = 0.2373070994;}
else if ( ChannelNumber == 364101) { NormSF = 0.0613837805;}
else if ( ChannelNumber == 364102) { NormSF = 0.02559761673;}
else if ( ChannelNumber == 364103) { NormSF = 0.02699790215;}
else if ( ChannelNumber == 364104) { NormSF = 0.02305794639;}
else if ( ChannelNumber == 364105) { NormSF = 0.00479731561;}
else if ( ChannelNumber == 364106) { NormSF = 0.006307109113;}
else if ( ChannelNumber == 364107) { NormSF = 0.003670030886;}
else if ( ChannelNumber == 364108) { NormSF = 0.0005609632939;}
else if ( ChannelNumber == 364109) { NormSF = 0.002200455925;}
else if ( ChannelNumber == 364110) { NormSF = 0.001932685014;}
else if ( ChannelNumber == 364111) { NormSF = 0.0006262526093;}
else if ( ChannelNumber == 364112) { NormSF = 0.0004677713162;}
else if ( ChannelNumber == 364113) { NormSF = 0.0001129649966;}
else if ( ChannelNumber == 364114) { NormSF = 0.2361986424;}
else if ( ChannelNumber == 364115) { NormSF = 0.06149661766;}
else if ( ChannelNumber == 364116) { NormSF = 0.02473445545;}
else if ( ChannelNumber == 364117) { NormSF = 0.02770299507;}
else if ( ChannelNumber == 364118) { NormSF = 0.02223521124;}
else if ( ChannelNumber == 364119) { NormSF = 0.004894907155;}
else if ( ChannelNumber == 364120) { NormSF = 0.006531077908;}
else if ( ChannelNumber == 364121) { NormSF = 0.003695372314;}
else if ( ChannelNumber == 364122) { NormSF = 0.0005584687629;}
else if ( ChannelNumber == 364123) { NormSF = 0.002225041315;}
else if ( ChannelNumber == 364124) { NormSF = 0.001961501031;}
else if ( ChannelNumber == 364125) { NormSF = 0.0006367687037;}
else if ( ChannelNumber == 364126) { NormSF = 0.0004796870994;}
else if ( ChannelNumber == 364127) { NormSF = 0.0001141794641;}
else if ( ChannelNumber == 364242) { NormSF = 0.003155340625;}
else if ( ChannelNumber == 364243) { NormSF = 0.002576282147;}
else if ( ChannelNumber == 364244) { NormSF = 0.002563030453;}
else if ( ChannelNumber == 364245) { NormSF = 0.001025432426;}
else if ( ChannelNumber == 364246) { NormSF = 0.0004556402054;}
else if ( ChannelNumber == 364247) { NormSF = 0.0003667993236;}
else if ( ChannelNumber == 364248) { NormSF = 0.00008053537019;}
else if ( ChannelNumber == 364249) { NormSF = 0.0001587500428;}
else if ( ChannelNumber == 410142) { NormSF = 0.00006849902419;}
else if ( ChannelNumber == 410155) { NormSF = 0.0001461660896;}
else if ( ChannelNumber == 410156) { NormSF = 0.0007410804503;}
else if ( ChannelNumber == 410157) { NormSF = 0.0003698116557;}
else if ( ChannelNumber == 410081) { NormSF = 0.00019808;}
else if ( ChannelNumber == 364250) { NormSF = 0.00008244273743;}
else if ( ChannelNumber == 364283) { NormSF = 0.00002468046757;}
else if ( ChannelNumber == 345706) { NormSF = 0.00001003655534;}
}

else if (year == 18)
{
     if ( ChannelNumber ==   341350 ){ NormSF =  0.000149059;}
else if ( ChannelNumber ==   341351 ){ NormSF =  0.000402269;}
else if ( ChannelNumber ==   341352 ){ NormSF =  0.000499277;}
else if ( ChannelNumber ==   341353 ){ NormSF =  0.000108999;}
else if ( ChannelNumber ==   341354 ){ NormSF =  0.00078382 ;}
else if ( ChannelNumber ==   341355 ){ NormSF =  0.001029866;}
else if ( ChannelNumber ==   341356 ){ NormSF =  0.001327682;}
else if ( ChannelNumber ==   341357 ){ NormSF =  0.000311825;}
else if ( ChannelNumber ==   341358 ){ NormSF =  0.002833662;}
else if ( ChannelNumber ==   341359 ){ NormSF =  0.004194965;} 
else if ( ChannelNumber ==   341360 ){ NormSF =  0.005932024;}
else if ( ChannelNumber ==   341361 ){ NormSF =  0.008101913;}
else if ( ChannelNumber ==   341362 ){ NormSF =  0.010962774;}
else if ( ChannelNumber ==   341364 ){ NormSF =  0.017912673;}
else if ( ChannelNumber ==   341367 ){ NormSF =  0.032704555;}
else if ( ChannelNumber ==   341368 ){ NormSF =  0.000218624;}
else if ( ChannelNumber ==   341369 ){ NormSF =  0.000830695;}
else if ( ChannelNumber ==   341370 ){ NormSF =  0.000849612;}
else if ( ChannelNumber ==   341371 ){ NormSF =  0.000223422;}
else if ( ChannelNumber ==   341372 ){ NormSF =  0.000831575;}
else if ( ChannelNumber ==   341373 ){ NormSF =  0.000834069;}
else if ( ChannelNumber ==   341374 ){ NormSF =  0.000827891;}
else if ( ChannelNumber ==   341375 ){ NormSF =  0.000233387;}
else if ( ChannelNumber ==   341376 ){ NormSF =  0.000831595;}
else if ( ChannelNumber ==   341377 ){ NormSF =  0.000829819;}
else if ( ChannelNumber ==   341378 ){ NormSF =  0.000849318;}
else if ( ChannelNumber ==   341379 ){ NormSF =  0.000751982;}
else if ( ChannelNumber ==   341380 ){ NormSF =  0.000887086;}
else if ( ChannelNumber ==   341382 ){ NormSF =  0.000826921;}
else if ( ChannelNumber ==   341385 ){ NormSF =  0.000814362;}
//if ( ChannelNumber == 341350 ) { NormSF = 0.00001930662665;}
//else if ( ChannelNumber == 341353 ) { NormSF = 0.000006598725596;}
//else if ( ChannelNumber == 341357 ) { NormSF = 0.0001117499371;}
//else if ( ChannelNumber == 341351 ) { NormSF = 0 ;}
//else if ( ChannelNumber == 341352 ) { NormSF = 0 ;}
//else if ( ChannelNumber == 341353 ) { NormSF = 0 ;}
//else if ( ChannelNumber == 341354 ) { NormSF = 0 ;}
//else if ( ChannelNumber == 341355 ) { NormSF = 0 ;}
//else if ( ChannelNumber == 341356 ) { NormSF = 0 ;}
//else if ( ChannelNumber == 341357 ) { NormSF = 0 ;}
//else if ( ChannelNumber == 341358 ) { NormSF = 0 ;}
//else if ( ChannelNumber == 341359 ) { NormSF = 0 ;}
//else if ( ChannelNumber == 341360 ) { NormSF = 0 ;}
//else if ( ChannelNumber == 341361 ) { NormSF = 0 ;}
//else if ( ChannelNumber == 341362 ) { NormSF = 0 ;}
//else if ( ChannelNumber == 341364 ) { NormSF = 0 ;}
//else if ( ChannelNumber == 341367 ) { NormSF = 0 ;}
//else if ( ChannelNumber == 341368 ) { NormSF = 0.002341786438;}
//else if ( ChannelNumber == 341369 ) { NormSF = 0.008769791447;}
//else if ( ChannelNumber == 341370 ) { NormSF = 0.008820715998;}
//else if ( ChannelNumber == 341371 ) { NormSF = 0.002363804738;}
//else if ( ChannelNumber == 341372 ) { NormSF = 0.008886836488;}
//else if ( ChannelNumber == 341373 ) { NormSF = 0.008636124707;}
//else if ( ChannelNumber == 341374 ) { NormSF = 0.008840109278;}
//else if ( ChannelNumber == 341375 ) { NormSF = 0.002442147385;}
//else if ( ChannelNumber == 341376 ) { NormSF = 0.00862936202;}
//else if ( ChannelNumber == 341377 ) { NormSF = 0.00858375506;}
//else if ( ChannelNumber == 341378 ) { NormSF = 0.00891574474;}
//else if ( ChannelNumber == 341379 ) { NormSF = 0.008407732529;}
//else if ( ChannelNumber == 341380 ) { NormSF = 0.009305958614;}
//else if ( ChannelNumber == 341382 ) { NormSF = 0.008348950118;}
//else if ( ChannelNumber == 341385 ) { NormSF = 0.008690327408;}
else if ( ChannelNumber == 345723 ) { NormSF = 0.0000417543635;}
else if ( ChannelNumber == 345666 ) { NormSF = 0.000842558009;}
else if ( ChannelNumber == 364283 ) { NormSF = 0.000000173882734;}
else if ( ChannelNumber == 364253 ) { NormSF = 0.0004956409194;}
else if ( ChannelNumber == 364284 ) { NormSF = 0.00001647731982;}
else if ( ChannelNumber == 364254 ) {
       if( WWtype == 0) NormSF = 0.001472955948;
       else if ( WWtype == -1) NormSF = 0.001472955948;
       else if ( WWtype == 1) NormSF = -0.001472955948*0.5;
       else if ( WWtype == 2) NormSF = 0.001472955948*1.5;
       if ( WWtype == -99) std::cout<<"Warning: NormSF got Wrong"<<std::endl;
      }
else if ( ChannelNumber == 345718 ) { NormSF = 0.000008585115314;}
else if ( ChannelNumber == 364285 ) { 
	       if( WWtype == 0) NormSF = 0.0001921134218;
       else if ( WWtype == -1) NormSF = 0.0001921134218;
       else if ( WWtype == 1) NormSF = -0.0001921134218*0.5;
       else if ( WWtype == 2) NormSF = 0.0001921134218*1.5;
       if ( WWtype == -99) std::cout<<"Warning: NormSF got Wrong"<<std::endl;
	}
else if ( ChannelNumber == 410472 ) { NormSF = 0.000001211206976;}
else if ( ChannelNumber == 410644 ) { NormSF = 0.0003077054791;}
else if ( ChannelNumber == 410645 ) { NormSF = 0.0003062482989;}
else if ( ChannelNumber == 410658 ) { NormSF = 0.000143828359;}
else if ( ChannelNumber == 410659 ) { NormSF = 0.0001426425186;}
else if ( ChannelNumber == 410646 ) { NormSF = 0.00001199049677;}
else if ( ChannelNumber == 410647 ) { NormSF = 0.00001204038695;}
else if ( ChannelNumber == 364128 ) { NormSF = 0.1777207346;}
else if ( ChannelNumber == 364129 ) { NormSF = 0.04437374335;}
else if ( ChannelNumber == 364130 ) { NormSF = 0.01857056857;}
else if ( ChannelNumber == 364131 ) { NormSF = 0.0204303449;}
else if ( ChannelNumber == 364132 ) { NormSF = 0.01695102476;}
else if ( ChannelNumber == 364133 ) { NormSF = 0.003440090696;}
else if ( ChannelNumber == 364134 ) { NormSF = 0.004948742207;}
else if ( ChannelNumber == 364135 ) { NormSF = 0.002793611714;}
else if ( ChannelNumber == 364136 ) { NormSF = 0.001076449317;}
else if ( ChannelNumber == 364137 ) { NormSF = 0.001665305627;}
else if ( ChannelNumber == 364138 ) { NormSF = 0.001463762491;}
else if ( ChannelNumber == 364139 ) { NormSF = 0.0004781538896;}
else if ( ChannelNumber == 364140 ) { NormSF = 0.0003581662367;}
else if ( ChannelNumber == 364141 ) { NormSF = 0.00008514744186;}
else if ( ChannelNumber == 364100 ) { NormSF = 0.1777564459;}
else if ( ChannelNumber == 364101 ) { NormSF = 0.0459320535;}
else if ( ChannelNumber == 364102 ) { NormSF = 0.01874375266;}
else if ( ChannelNumber == 364103 ) { NormSF = 0.02029269671;}
else if ( ChannelNumber == 364104 ) { NormSF = 0.0174422592;}
else if ( ChannelNumber == 364105 ) { NormSF = 0.00360317778;}
else if ( ChannelNumber == 364106 ) { NormSF = 0.004754877256;}
else if ( ChannelNumber == 364107 ) { NormSF = 0.002750984033;}
else if ( ChannelNumber == 364108 ) { NormSF = 0.0004229784916;}
else if ( ChannelNumber == 364109 ) { NormSF = 0.001625299753;}
else if ( ChannelNumber == 364110 ) { NormSF = 0.001445850857;}
else if ( ChannelNumber == 364111 ) { NormSF = 0.0004704253281;}
else if ( ChannelNumber == 364112 ) { NormSF = 0.0003431589009;}
else if ( ChannelNumber == 364113 ) { NormSF = 0.00008869495096;}
else if ( ChannelNumber == 364114 ) { NormSF = 0.1779613381;}
else if ( ChannelNumber == 364115 ) { NormSF = 0.04618202967;}
else if ( ChannelNumber == 364116 ) { NormSF = 0.0186109729;}
else if ( ChannelNumber == 364117 ) { NormSF = 0.02057987738;}
else if ( ChannelNumber == 364118 ) { NormSF = 0.01669411842;}
else if ( ChannelNumber == 364119 ) { NormSF = 0.003706941537;}
else if ( ChannelNumber == 364120 ) { NormSF = 0.004879597544;}
else if ( ChannelNumber == 364121 ) { NormSF = 0.00277907863;}
else if ( ChannelNumber == 364122 ) { NormSF = 0.0004212082728;}
else if ( ChannelNumber == 364123 ) { NormSF = 0.001622745734;}
else if ( ChannelNumber == 364124 ) { NormSF = 0.001411970152;}
else if ( ChannelNumber == 364125 ) { NormSF = 0.0004899000124;}
else if ( ChannelNumber == 364126 ) { NormSF = 0.0003578213821;}
else if ( ChannelNumber == 364127 ) { NormSF = 0.00008542551701;}
else if ( ChannelNumber == 364242 ) { NormSF = 0.001897414107;}
else if ( ChannelNumber == 364243 ) { NormSF = 0.001539648271;}
else if ( ChannelNumber == 364244 ) { NormSF = 0.001536520446;}
else if ( ChannelNumber == 364245 ) { NormSF = 0.0005664020013;}
else if ( ChannelNumber == 364246 ) { NormSF = 0.0002530614584;}
else if ( ChannelNumber == 364247 ) { NormSF = 0.0001833659741;}
else if ( ChannelNumber == 364248 ) { NormSF = 0.00004011988443;}
else if ( ChannelNumber == 364249 ) { NormSF = 0.00007886045914;}
else if ( ChannelNumber == 364250 ) { NormSF = 0.0001158267037;}
else if ( ChannelNumber == 364283 ) { NormSF = 0.000000173882734;}
else if ( ChannelNumber == 345706 ) { NormSF = 0.00001014750369;}
else if ( ChannelNumber == 410155 ) { NormSF = 0.00009104044035;}
else if ( ChannelNumber == 410156 ) { NormSF = 0.0005546774767;}
else if ( ChannelNumber == 410157 ) { NormSF = 0.0003092807501;}
else if ( ChannelNumber == 410081 ) { NormSF = 0.00019808             ;}
}
return NormSF;
}

