#include "NtupleMaker.h"

void string_replace( std::string &strBig, const std::string &strsrc, const std::string &strdst) {
    std::string::size_type pos = 0;
    std::string::size_type srclen = strsrc.size();
    std::string::size_type dstlen = strdst.size();

    while( (pos=strBig.find(strsrc, pos)) != std::string::npos ) {
        strBig.replace( pos, srclen, strdst );
        pos += dstlen;
    }
}
std::string GetPathOrURLShortName(std::string strFullName) {
    if (strFullName.empty()) {
        return "";
    }

    string_replace(strFullName, "/", "\\");

    std::string::size_type iPos = strFullName.find_last_of('\\') + 1;

    return strFullName.substr(iPos, strFullName.length() - iPos);
}


double CaldPhi(double phi1, double phi2) {
    double Dphi = 0;
    Dphi = phi1 - phi2;
    if(fabs(Dphi) > Pi) {
        if( Dphi>0 ) {
            Dphi = 2*Pi-Dphi;
        } else {
            Dphi = 2*Pi+Dphi;
        }
    }
    return Dphi;
}


double NtupleMaker::Lum(string INST) {

    if ( INST.find("364156")!= string::npos)   mcChannelNumber =  364156;
    if ( INST.find("364157")!= string::npos)   mcChannelNumber =  364157;
    if ( INST.find("364158")!= string::npos)   mcChannelNumber =  364158;
    if ( INST.find("364159")!= string::npos)   mcChannelNumber =  364159;
    if ( INST.find("364160")!= string::npos)   mcChannelNumber =  364160;
    if ( INST.find("364161")!= string::npos)   mcChannelNumber =  364161;
    if ( INST.find("364162")!= string::npos)   mcChannelNumber =  364162;
    if ( INST.find("364163")!= string::npos)   mcChannelNumber =  364163;
    if ( INST.find("364164")!= string::npos)   mcChannelNumber =  364164;
    if ( INST.find("364165")!= string::npos)   mcChannelNumber =  364165;
    if ( INST.find("364166")!= string::npos)   mcChannelNumber =  364166;
    if ( INST.find("364167")!= string::npos)   mcChannelNumber =  364167;
    if ( INST.find("364168")!= string::npos)   mcChannelNumber =  364168;
    if ( INST.find("364169")!= string::npos)   mcChannelNumber =  364169;
    if ( INST.find("364170")!= string::npos)   mcChannelNumber =  364170;
    if ( INST.find("364171")!= string::npos)   mcChannelNumber =  364171;
    if ( INST.find("364172")!= string::npos)   mcChannelNumber =  364172;
    if ( INST.find("364173")!= string::npos)   mcChannelNumber =  364173;
    if ( INST.find("364174")!= string::npos)   mcChannelNumber =  364174;
    if ( INST.find("364175")!= string::npos)   mcChannelNumber =  364175;
    if ( INST.find("364176")!= string::npos)   mcChannelNumber =  364176;
    if ( INST.find("364177")!= string::npos)   mcChannelNumber =  364177;
    if ( INST.find("364178")!= string::npos)   mcChannelNumber =  364178;
    if ( INST.find("364179")!= string::npos)   mcChannelNumber =  364179;
    if ( INST.find("364180")!= string::npos)   mcChannelNumber =  364180;
    if ( INST.find("364181")!= string::npos)   mcChannelNumber =  364181;
    if ( INST.find("364182")!= string::npos)   mcChannelNumber =  364182;
    if ( INST.find("364183")!= string::npos)   mcChannelNumber =  364183;
    if ( INST.find("364184")!= string::npos)   mcChannelNumber =  364184;
    if ( INST.find("364185")!= string::npos)   mcChannelNumber =  364185;
    if ( INST.find("364186")!= string::npos)   mcChannelNumber =  364186;
    if ( INST.find("364187")!= string::npos)   mcChannelNumber =  364187;
    if ( INST.find("364188")!= string::npos)   mcChannelNumber =  364188;
    if ( INST.find("364189")!= string::npos)   mcChannelNumber =  364189;
    if ( INST.find("364190")!= string::npos)   mcChannelNumber =  364190;
    if ( INST.find("364191")!= string::npos)   mcChannelNumber =  364191;
    if ( INST.find("364192")!= string::npos)   mcChannelNumber =  364192;
    if ( INST.find("364193")!= string::npos)   mcChannelNumber =  364193;
    if ( INST.find("364194")!= string::npos)   mcChannelNumber =  364194;
    if ( INST.find("364195")!= string::npos)   mcChannelNumber =  364195;
    if ( INST.find("364196")!= string::npos)   mcChannelNumber =  364196;
    if ( INST.find("364197")!= string::npos)   mcChannelNumber =  364197;
    if ( INST.find("361106")!= string::npos)   mcChannelNumber =  361106;
    if ( INST.find("361107")!= string::npos)   mcChannelNumber =  361107;
    if ( INST.find("361108")!= string::npos)   mcChannelNumber =  361108;
    if ( INST.find("410470")!= string::npos)   mcChannelNumber =  410470;
    if ( INST.find("410471")!= string::npos)   mcChannelNumber =  410471;
    if ( INST.find("410472")!= string::npos)   mcChannelNumber =  410472;
    if ( INST.find("410644")!= string::npos)   mcChannelNumber =  410644;
    if ( INST.find("410645")!= string::npos)   mcChannelNumber =  410645;
    if ( INST.find("410646")!= string::npos)   mcChannelNumber =  410646;
    if ( INST.find("410647")!= string::npos)   mcChannelNumber =  410647;
    if ( INST.find("410658")!= string::npos)   mcChannelNumber =  410658;
    if ( INST.find("410659")!= string::npos)   mcChannelNumber =  410659;
    if ( INST.find("364250")!= string::npos)   mcChannelNumber =  364250;
    if ( INST.find("364253")!= string::npos)   mcChannelNumber =  364253;
    if ( INST.find("364254")!= string::npos)   mcChannelNumber =  364254;
    if ( INST.find("364255")!= string::npos)   mcChannelNumber =  364255;
    if ( INST.find("363355")!= string::npos)   mcChannelNumber =  363355;
    if ( INST.find("363356")!= string::npos)   mcChannelNumber =  363356;
    if ( INST.find("363357")!= string::npos)   mcChannelNumber =  363357;
    if ( INST.find("363358")!= string::npos)   mcChannelNumber =  363358;
    if ( INST.find("363359")!= string::npos)   mcChannelNumber =  363359;
    if ( INST.find("363360")!= string::npos)   mcChannelNumber =  363360;
    if ( INST.find("363489")!= string::npos)   mcChannelNumber =  363489;

    if (( INST.find("MC1516") != string::npos )||( INST.find("MC16a") != string::npos )) {
        //        lum =36.207660;
        luminosity =3.219+32.988;
//        Yeard =16;
    } else if ( INST.find("MC16d") != string::npos ) {
        //        lum =44.3074;
        luminosity =44.307;
//        Yeard = 17;
    } else if ( INST.find("MC16e") != string::npos ) {
        //        lum =59.9372;
        luminosity =58.450;
//        Yeard = 18;
    } else {
        cout<<"MCSample Not 16a 16d or 16e"<<endl;
    }
        return luminosity;
}

NtupleMaker::NtupleMaker(string Input, bool SampleType) {


//    int Yeard = -1;
//    wrkarea = "/eos/user/j/jugao/LFVWORK/SkimedRoot/1516/";
    TH1::SetDefaultSumw2();
    string Inos;
    ifstream os;
    isData = SampleType;
    os.open(Input.c_str());



    while(os >> Inos) {
        InputFileList.push_back(Inos);
    }
    if (( Input.length() > 20) || Input.find("/") != string::npos) {
        Input = GetPathOrURLShortName(Input);
    }
    filename = 0;
    Input.erase(Input.length()-5);
    OutPutName = Input;




//    cout<<Input<<endl;
//    luminosity = 43813.7*0.001;
    NormSF = 1;
}





void NtupleMaker::ReadTreeLFVO( TChain *chain ) {
    //    TFile *f1 = (TFile *)gROOT->GetListOfFiles()->FindObject((inFileName).c_str());
//    TFile *f1 = new TFile(inFileName.c_str(),"READ");
//    ssTree = (TTree *)f1->Get("LFV");
    chain->SetBranchAddress("EventWeight",&(EventWeight));
    chain->SetBranchAddress("PrwWeight",&(PrwWeight));
    chain->SetBranchAddress("MCWeight",&(MCWeight));
    chain->SetBranchAddress("Event_Trigger_Match",&(Event_Trigger_Match));
    chain->SetBranchAddress("Event_Trigger_Pass",&(Event_Trigger_Pass));
    chain->SetBranchAddress("RunNo",&(RunNo));
    chain->SetBranchAddress("lumB",&(lumB));
    chain->SetBranchAddress("EventTag",&(EventTag));
    chain->SetBranchAddress("eventNumber",&(eventNumber));
    chain->SetBranchAddress("dPhi_ej",&(dPhi_ej));
    chain->SetBranchAddress("dPhi_ll",&(dPhi_ll));
    chain->SetBranchAddress("isKeep",&(isKeepEvent));
    chain->SetBranchAddress("xsec",&(xsec));
    chain->SetBranchAddress("isEvtClean",&(isEvtClean));
    chain->SetBranchAddress("isVertexGood",&(isVertexGood));
    chain->SetBranchAddress("isTrigPass",&(isTrigPass));
    chain->SetBranchAddress("MET_Et",&(MET_Et));
    chain->SetBranchAddress("MET_Phi",&(MET_Phi));
    chain->SetBranchAddress("Event_met", &(event_met));
    chain->SetBranchAddress("Event_met_Eta", &(event_met_eta));
    chain->SetBranchAddress("Event_met_Phi", &(event_met_phi));
    chain->SetBranchAddress("Event_met_M", &(event_met_m));
    chain->SetBranchAddress("Event_mT",&(event_mT));
    chain->SetBranchAddress("Ele_OQ",&(Ele_OQ));
    chain->SetBranchAddress("Ele_Charge",&(Ele_Charge));
    chain->SetBranchAddress("Ele_pt",&(Ele_pt));
    chain->SetBranchAddress("Ele_eta",&(Ele_eta));
    chain->SetBranchAddress("Ele_eta_calo",&(Ele_eta_calo));
    chain->SetBranchAddress("Ele_phi",&(Ele_phi));
    chain->SetBranchAddress("Ele_d0sig",&(Ele_d0sig));
    chain->SetBranchAddress("Ele_dz0",&(Ele_dz0));
    chain->SetBranchAddress("Ele_d0pass",&(Ele_d0pass));
    chain->SetBranchAddress("Ele_z0pass",&(Ele_z0pass));
    chain->SetBranchAddress("Ele_MuonOLR",&(Ele_MuonOLR));
    chain->SetBranchAddress("Ele_ID_Medium",&(Ele_ID_Medium));
    chain->SetBranchAddress("Ele_ID_Tight",&(Ele_ID_Tight));
    chain->SetBranchAddress("Ele_ID_Loose",&(Ele_ID_Loose));
    chain->SetBranchAddress("Ele_Iso",&(Ele_Iso));
    chain->SetBranchAddress("Ele_Iso_Loose",&(Ele_Iso_Loose));
    chain->SetBranchAddress("Ele_isTrigMch",&(Ele_isTrigMch));
    chain->SetBranchAddress("Ele_TrigEff",&(Ele_TrigEff));
    chain->SetBranchAddress("Ele_RecoSF",&(Ele_RecoSF));
    chain->SetBranchAddress("Ele_IsoSF",&(Ele_IsoSF));
    chain->SetBranchAddress("Ele_IDSF",&(Ele_IDSF));
    chain->SetBranchAddress("Ele_L1CaloSF",&(Ele_L1CaloSF));
    chain->SetBranchAddress("Mu_pt",&(Mu_pt));
    chain->SetBranchAddress("Mu_eta",&(Mu_eta));
    chain->SetBranchAddress("Mu_phi",&(Mu_phi));
    chain->SetBranchAddress("Mu_d0sig",&(Mu_d0sig));
    chain->SetBranchAddress("Mu_dz0",&(Mu_dz0));
    chain->SetBranchAddress("Mu_d0pass",&(Mu_d0pass));
    chain->SetBranchAddress("Mu_z0pass",&(Mu_z0pass));
    chain->SetBranchAddress("Mu_charge",&(Mu_charge));
    chain->SetBranchAddress("Mu_isHighPt",&(Mu_isHighPt));
    chain->SetBranchAddress("Mu_isID",&(Mu_isID));
    chain->SetBranchAddress("Mu_Iso",&(Mu_Iso));
    chain->SetBranchAddress("Mu_TrigSF",&(Mu_TrigSF));
    chain->SetBranchAddress("Mu_RecoSF",&(Mu_RecoSF));
    chain->SetBranchAddress("Mu_TrackSF",&(Mu_TrackSF));
    chain->SetBranchAddress("Mu_IsoSF",&(Mu_IsoSF));
    chain->SetBranchAddress("Mu_isTrigMch",&(Mu_isTrigMch));
    chain->SetBranchAddress("Mu_isOverlap",&(Mu_isOverlap));
    chain->SetBranchAddress("Tau_isOverlap",&(Tau_isOverlap));
    chain->SetBranchAddress("Tau_pt",&(Tau_pt));
    chain->SetBranchAddress("Tau_eta",&(Tau_eta));
    chain->SetBranchAddress("Tau_phi",&(Tau_phi));
    chain->SetBranchAddress("Tau_ntracks",&(Tau_ntracks));
    chain->SetBranchAddress("Tau_charge",&(Tau_charge));
    chain->SetBranchAddress("TauSelector",&(TauSelector));
    chain->SetBranchAddress("TauSelector_WithOLR",&(TauSelector_WithOLR));
    chain->SetBranchAddress("Tau_RecoSF",&(Tau_RecoSF));
    chain->SetBranchAddress("Tau_IDSF",&(Tau_IDSF));
    chain->SetBranchAddress("Tau_EleOLRSF",&(Tau_EleOLRSF));
    chain->SetBranchAddress("Jet_pt",&(Jet_pt));
    chain->SetBranchAddress("Jet_eta",&(Jet_eta));
    chain->SetBranchAddress("Jet_phi",&(Jet_phi));
    chain->SetBranchAddress("Jet_m",&(Jet_m));
    chain->SetBranchAddress("Jet_isGoodB",&(Jet_isGoodB));
    chain->SetBranchAddress("Jet_JVTSF",&(Jet_JVTSF));
    chain->SetBranchAddress("Jet_BTInefSF",&(Jet_BTInefSF));
    chain->SetBranchAddress("Jet_BTSF",&(Jet_BTSF));
    chain->SetBranchAddress("N_jets",&(N_jets));
    chain->SetBranchAddress("CleanEventNum",&(m_numCleanEvents ));
    chain->SetBranchAddress("Mu_TriggerMatched",&(Mu_TriggerMatched));
    chain->SetBranchAddress("Ele_TriggerMatched",&(Ele_TriggerMatched));
    chain->SetBranchAddress("Ele_isTight",&(Ele_isTight));
    chain->SetBranchAddress("Ele_isLoose",&(Ele_isLoose));
    chain->SetBranchAddress("Ele_isLoose_IDLoose",&(Ele_isLoose_IDLoose));


}

double NtupleMaker::FindXsec(double mcChannelNumber){
double XSEC = 1;
    if( mcChannelNumber == 364156.0 ) XSEC =  19143.0E+03*0.8238*0.9702;
    else if( mcChannelNumber == 364157.0 ) XSEC =  19121.0E+03*0.1304*0.9702;
    else if( mcChannelNumber == 364158.0 ) XSEC =  19135.0E+03*0.044118*0.9702;
    else if( mcChannelNumber == 364159.0 ) XSEC =  944.85E+03*0.67463*0.9702;
    else if( mcChannelNumber == 364160.0 ) XSEC =  937.78E+03*0.23456*0.9702;
    else if( mcChannelNumber == 364161.0 ) XSEC =  944.63E+03*0.075648*0.9702;
    else if( mcChannelNumber == 364162.0 ) XSEC =  339.54E+03*0.62601*0.9702;
    else if( mcChannelNumber == 364163.0 ) XSEC =  340.06E+03*0.28947*0.9702;
    else if( mcChannelNumber == 364164.0 ) XSEC =  339.54E+03*0.10872*0.9702;
    else if( mcChannelNumber == 364165.0 ) XSEC =  72.067E+03*0.54647*0.9702;
    else if( mcChannelNumber == 364166.0 ) XSEC =  72.198E+03*0.31743*0.9702;
    else if( mcChannelNumber == 364167.0 ) XSEC =  72.045E+03*0.13337*0.9702;
    else if( mcChannelNumber == 364168.0 ) XSEC =  15.01E+03*1.0*0.9702;
    else if( mcChannelNumber == 364169.0 ) XSEC =  1.2344E+03*1.0*0.9702;
    else if( mcChannelNumber == 364170.0 ) XSEC =  19127.0E+03*0.82447*0.9702;
    else if( mcChannelNumber == 364171.0 ) XSEC =  19130.0E+03*0.1303*0.9702;
    else if( mcChannelNumber == 364172.0 ) XSEC =  19135.0E+03*0.044141*0.9702;
    else if( mcChannelNumber == 364173.0 ) XSEC =  942.58E+03*0.66872*0.9702;
    else if( mcChannelNumber == 364174.0 ) XSEC =  945.67E+03*0.22787*0.9702;
    else if( mcChannelNumber == 364175.0 ) XSEC =  945.15E+03*0.10341*0.9702;
    else if( mcChannelNumber == 364176.0 ) XSEC =  339.81E+03*0.59691*0.9702;
    else if( mcChannelNumber == 364177.0 ) XSEC =  339.87E+03*0.28965*0.9702;
    else if( mcChannelNumber == 364178.0 ) XSEC =  339.48E+03*0.10898*0.9702;
    else if( mcChannelNumber == 364179.0 ) XSEC =  72.084E+03*0.54441*0.9702;
    else if( mcChannelNumber == 364180.0 ) XSEC =  72.128E+03*0.31675*0.9702;
    else if( mcChannelNumber == 364181.0 ) XSEC =  72.113E+03*0.13391*0.9702;
    else if( mcChannelNumber == 364182.0 ) XSEC =  15.224E+03*1.0*0.9702;
    else if( mcChannelNumber == 364183.0 ) XSEC =  1.2334E+03*1.0*0.9702;
    else if( mcChannelNumber == 364184.0 ) XSEC =  19152.0E+03*0.82495*0.9702;
    else if( mcChannelNumber == 364185.0 ) XSEC =  19153.0E+03*0.12934*0.9702;
    else if( mcChannelNumber == 364186.0 ) XSEC =  19163.0E+03*0.044594*0.9702;
    else if( mcChannelNumber == 364187.0 ) XSEC =  947.65E+03*0.67382*0.9702;
    else if( mcChannelNumber == 364188.0 ) XSEC =  946.73E+03*0.22222*0.9702;
    else if( mcChannelNumber == 364189.0 ) XSEC =  943.3E+03*0.10391*0.9702;
    else if( mcChannelNumber == 364190.0 ) XSEC =  339.36E+03*0.59622*0.9702;
    else if( mcChannelNumber == 364191.0 ) XSEC =  339.63E+03*0.29025*0.9702;
    else if( mcChannelNumber == 364192.0 ) XSEC =  339.54E+03*0.11799*0.9702;
    else if( mcChannelNumber == 364193.0 ) XSEC =  72.065E+03*0.54569*0.9702;
    else if( mcChannelNumber == 364194.0 ) XSEC =  71.976E+03*0.31648*0.9702;
    else if( mcChannelNumber == 364195.0 ) XSEC =  72.026E+03*0.13426*0.9702;
    else if( mcChannelNumber == 364196.0 ) XSEC =  15.046E+03*1.0*0.9702;
    else if( mcChannelNumber == 364197.0 ) XSEC =  1.2339E+03*1.0*0.9702;
    else if( mcChannelNumber == 361106.0 ) XSEC = 1.9011E+06*1.026;
    else if( mcChannelNumber == 361107.0 ) XSEC = 1.9011E+06*1.026;
    else if( mcChannelNumber == 361108.0 ) XSEC = 1.9011E+06*1.026;
    else if( mcChannelNumber == 410470.0 ) XSEC = 0.54383*831.76*1E+03;
    else if( mcChannelNumber == 410471.0 ) XSEC = 0.45627*1.1398*729.9*1E+03;
    else if( mcChannelNumber == 410472.0 ) XSEC = 0.10546*1.1398*729.9*1E+03;
    else if( mcChannelNumber == 410644.0 ) XSEC = 2.0267e+03*1.0170;
    else if( mcChannelNumber == 410645.0 ) XSEC = 1.2675e+03*1.0167;
    else if( mcChannelNumber == 410646.0 ) XSEC = 3.7937E+04*0.9450;
    else if( mcChannelNumber == 410647.0 ) XSEC = 3.7907E+04*0.9457;
    else if( mcChannelNumber == 410658.0 ) XSEC = 3.6993E+04*1.1935;
    else if( mcChannelNumber == 410659.0 ) XSEC = 2.2175E+04*1.1849;
    else if( mcChannelNumber == 364250.0 ) XSEC =   1252.3;
    else if( mcChannelNumber == 364253.0 ) XSEC =   4578.1;
    else if( mcChannelNumber == 364254.0 ) XSEC =   12504.;
    else if( mcChannelNumber == 364255.0 ) XSEC =   3234.4;
    else if( mcChannelNumber == 363355.0 ) XSEC =   0.27978*15560.;
    else if( mcChannelNumber == 363356.0 ) XSEC =   0.14089*15561.;
    else if( mcChannelNumber == 363357.0 ) XSEC =   6787.4 ;
    else if( mcChannelNumber == 363358.0 ) XSEC =   3433;
    else if( mcChannelNumber == 363359.0 ) XSEC =   24708.;
    else if( mcChannelNumber == 363360.0 ) XSEC =   24725.;
    else if( mcChannelNumber == 363489.0 ) XSEC =   11419.;
    else (cout << "ERROR :: MC Channel Not Found!!!!!" << endl);
    return XSEC;
}

double NtupleMaker::CalLumNormSF(double mcChannelNumber) {

    double Weight = -1;
    if( mcChannelNumber == 364156.0 ) Weight =  19149.0E+03/(1.67834e+07);
    if( mcChannelNumber == 364157.0 ) Weight =  19144.0E+03/(4.49172e+06);
    if( mcChannelNumber == 364158.0 ) Weight =  19138.0E+03/(1.05598e+07);
    if( mcChannelNumber == 364159.0 ) Weight =  945.52E+03/(5.48221e+06);
    if( mcChannelNumber == 364160.0 ) Weight =  945.38E+03/(3.73974e+06);
    if( mcChannelNumber == 364161.0 ) Weight =  944.8E+03/(8.10169e+06);
    if( mcChannelNumber == 364162.0 ) Weight =  339.73E+03/(6.22887e+06);
    if( mcChannelNumber == 364163.0 ) Weight =  339.8E+03/(5.32602e+06);
    if( mcChannelNumber == 364164.0 ) Weight =  339.64E+03/(949436);
    if( mcChannelNumber == 364165.0 ) Weight =  72.079E+03/(3.82419e+06);
    if( mcChannelNumber == 364166.0 ) Weight =  72.1E+03/(1.25793e+06);
    if( mcChannelNumber == 364167.0 ) Weight =  72.058E+03/(2.81664e+06);
    if( mcChannelNumber == 364168.0 ) Weight =  15.006E+03/(6.02934e+06);
    if( mcChannelNumber == 364169.0 ) Weight =  1.2348E+03/(4.10815e+06);
    if( mcChannelNumber == 364170.0 ) Weight =  19153.0E+03/(1.67884e+07);
    if( mcChannelNumber == 364171.0 ) Weight =  19144.0E+03/(5.67946e+06);
    if( mcChannelNumber == 364172.0 ) Weight =  19138.0E+03/(1.05538e+07);
    if( mcChannelNumber == 364173.0 ) Weight =  944.98E+03/(5.41599e+06);
    if( mcChannelNumber == 364174.0 ) Weight =  945.74E+03/(3.76077e+06);
    if( mcChannelNumber == 364175.0 ) Weight =  945.77E+03/(4.01777e+06);
    if( mcChannelNumber == 364176.0 ) Weight =  339.75E+03/(6.2347e+06);
    if( mcChannelNumber == 364177.0 ) Weight =  339.8E+03/(5.32607e+06);
    if( mcChannelNumber == 364178.0 ) Weight =  339.64E+03/(1.84335e+07);
    if( mcChannelNumber == 364179.0 ) Weight =  72.074E+03/(4.37841e+06);
    if( mcChannelNumber == 364180.0 ) Weight =  72.105E+03/(2.8125e+06);
    if( mcChannelNumber == 364181.0 ) Weight =  72.091E+03/(2.87353e+06);
    if( mcChannelNumber == 364182.0 ) Weight =  15.047E+03/(6.08545e+06);
    if( mcChannelNumber == 364183.0 ) Weight =  1.2344E+03/(4.12914e+06);
    if( mcChannelNumber == 364184.0 ) Weight =  19155.0E+03/(1.68192e+07);
    if( mcChannelNumber == 364185.0 ) Weight =  19153.0E+03/(5.74535e+06);
    if( mcChannelNumber == 364186.0 ) Weight =  19144.0E+03/(1.06269e+07);
    if( mcChannelNumber == 364187.0 ) Weight =  945.02E+03/(5.49649e+06);
    if( mcChannelNumber == 364188.0 ) Weight =  946.23E+03/(1.25802e+06);
    if( mcChannelNumber == 364189.0 ) Weight =  945.71E+03/(4.02645e+06);
    if( mcChannelNumber == 364190.0 ) Weight =  339.69E+03/(6.21047e+06);
    if( mcChannelNumber == 364191.0 ) Weight =  339.84E+03/(5.31502e+06);
    if( mcChannelNumber == 364192.0 ) Weight =  339.31E+03/(1.85371e+07);
    if( mcChannelNumber == 364193.0 ) Weight =  72.078E+03/(4.38234e+06);
    if( mcChannelNumber == 364194.0 ) Weight =  71.99E+03/(2.81152e+06);
    if( mcChannelNumber == 364195.0 ) Weight =  71.945E+03/(2.87229e+06);
    if( mcChannelNumber == 364196.0 ) Weight =  15.052E+03/(5.99811e+06);
    if( mcChannelNumber == 364197.0 ) Weight =  1.2341E+03/(4.11312e+06);
    if( mcChannelNumber == 361106.0 ) Weight = 1.901E+06/(1.51982e+11);
    if( mcChannelNumber == 361107.0 ) Weight = 1.901E+06/(1.51852e+11);
    if( mcChannelNumber == 361108.0 ) Weight = 1.9E+06/(7.50837e+10);
    if( mcChannelNumber == 410470.0 ) Weight = 0.54383*1.1398*729.9*1E+03/(8.72058e+10);
    if( mcChannelNumber == 410471.0 ) Weight = 0.45627*1.1398*729.9*1E+03/(2.91241e+10);
    if( mcChannelNumber == 410472.0 ) Weight = 0.10546*1.1398*729.9*1E+03/(1.43901e+10);
    if( mcChannelNumber == 410644.0 ) Weight = 2.0267e+03/(4.05637e+06);
    if( mcChannelNumber == 410645.0 ) Weight = 1.2674e+03/(2.53797e+06);
    if( mcChannelNumber == 410646.0 ) Weight = 3.7936E+04/(1.89796e+08);
    if( mcChannelNumber == 410647.0 ) Weight = 3.7905E+04/(1.89608e+08);
    if( mcChannelNumber == 410658.0 ) Weight = 3.6996E+04/(1.77371e+08);
    if( mcChannelNumber == 410659.0 ) Weight = 2.2175E+04/(1.05688e+08);
    if( mcChannelNumber == 364250.0 ) Weight =   1252.3 /(7.51881e+06);
    if( mcChannelNumber == 364253.0 ) Weight =   4579./(4.55024e+06);
    if( mcChannelNumber == 364254.0 ) Weight =   12501./(5.1148e+06);
    if( mcChannelNumber == 364255.0 ) Weight =   3234.4/(1.75046e+06);
//    if( mcChannelNumber == 363355.0 ) Weight =   0.27686*15560./(0);
//    if( mcChannelNumber == 363356.0 ) Weight =   0.14158*15564./(0);
    if( mcChannelNumber == 363357.0 ) Weight =   6795./1/(559370);
    if( mcChannelNumber == 363358.0 ) Weight =   3432.8/(254404);
    if( mcChannelNumber == 363359.0 ) Weight =   24708./(1.07634e+06);
    if( mcChannelNumber == 363360.0 ) Weight =   24724./(1.08043e+06);
    cout <<"NSF"<<luminosity<<endl;
    return luminosity*Weight;

}


double NtupleMaker::CalLumNormSF(TChain * chain, double xSec) {
//    TFile *file2 = new TFile(inFileName.c_str(),"READ");
//    TTree *meta = (TTree *)file2->Get("metaTree");
    chain->SetBranchAddress("totalWeightedEntries", &(sumOfWeights));
    chain->SetBranchAddress("inputFileName", &(filename));
    vector<string> fileList;
    double Num=0;
    for(Long64_t i=0; i<chain->GetEntries(); i++) {
        chain->GetEntry(i);
        bool isDuplicated = false;
        for(auto na : fileList) {
            if(*(filename)==na) {
                isDuplicated = true;
                break;
            }
        }
        if(!isDuplicated) {
            fileList.push_back(*(filename));
            Num+=(sumOfWeights);
//            cout<<"bookkeeping "<<(filename)<<endl;
        }
    }
    cout <<"lum: "<<luminosity<<endl;
    cout <<"Xsec: "<<xSec<<endl;
    cout <<"SOW: "<<Num<<endl;
    fileList.clear();
//    delete meta;
//    file2->Close();
    return luminosity*xSec/Num;
}

void NtupleMaker::InitialVarible() {
    ele_pt = 0;
    ele_eta = 0;
    ele_phi = 0;
//    isData = 0;
    ele_isTight = 0;
    weight = 0;
    mu_pt = 0;
    mu_eta = 0;
    mu_phi = 0;
    mu_isTight = 0;
    tau_pt = 0;
    tau_eta = 0;
    tau_phi = 0;
    tau_isTight = 0;
    muonTrigSF =1.0;
    muonRecoSF = 1.0;
    muonIsoSF = 1.0;
    muonTtvaSF = 1.0;
    electronTrigSF = 1.0;
    electronIDSF = 1.0;
    electronRecoSF = 1.0;
    electronIsoSF = 1.0;
    electronL1CaloSF = 1.0;
    tauRecoSF = 1.0;
    tauEleOlrSF = 1.0;
    tauEJetIDSF = 1.0;
//    xsecSF = 0;
//    lumSF = 0;
//    NormSF = 0;
    Etag = -1;
    jetJvtSF = 1.0;
//    luminosity = 0;
}


void NtupleMaker::SkimProcessor( string inFileName,TChain *chain ) {
//    cout<<(wrkarea+inFileName+"_Skim.root").c_str()<<endl;
//    TFile* ntupleF = new TFile((wrkarea+inFileName+"_Skim.root").c_str(),"recreate");
    TFile* ntupleF = new TFile((inFileName+"_Skim.root").c_str(),"recreate");
    EMuTauTree = new TTree("Emutau","Emutau");
    EMuTauTree->SetDirectory(ntupleF);
    EMuTauTree->Branch("ele_pt",&ele_pt);
    EMuTauTree->Branch("ele_eta",&ele_eta);
    EMuTauTree->Branch("ele_phi",&ele_phi);
    EMuTauTree->Branch("ele_isTight",&ele_isTight);
    EMuTauTree->Branch("mu_pt",&mu_pt);
    EMuTauTree->Branch("mu_eta",&mu_eta);
    EMuTauTree->Branch("mu_phi",&mu_phi);
    EMuTauTree->Branch("mu_isTight",&mu_isTight);
    EMuTauTree->Branch("tau_pt",&tau_pt);
    EMuTauTree->Branch("tau_eta",&tau_eta);
    EMuTauTree->Branch("tau_phi",&tau_phi);
    EMuTauTree->Branch("tau_isTight",&tau_isTight);
    EMuTauTree->Branch("isData",&isData);
    EMuTauTree->Branch("NormSF",&NormSF);
    EMuTauTree->Branch("weight",&weight);
    EMuTauTree->Branch("Etag",&Etag);
    EMuTauTree->Branch("DPhi_ll",&DPhi_ll);

    cout<<chain->GetEntries()<<endl;
    int cnt0 = 0;
    int cnt1 = 0;
//    int cnt2 = 0;
//    int cnt3 = 0;
//    int cntemu = 0;
//    int cntetau = 0;
//    int cntmutau = 0;
//    int cntemu0 = 0;
//    int cntetau0 = 0;
//    int cntmutau0 = 0;
//    int cntemu1 = 0;
//    int cntetau1 = 0;
//    int cntmutau1 = 0;
    for(int ii=0; ii<chain->GetEntries(); ii++) {
        chain->GetEntry(ii);
        InitialVarible();

        if(ii == 0) {
            if (isData) {
                CalNMSF = 1;
            } else {
                double XSection = FindXsec(mcChannelNumber);
                CalNMSF = CalLumNormSF(meta,XSection);
//                CalNMSF = CalLumNormSF(mcChannelNumber);
                cout << "NormSF: "<<CalNMSF <<endl;
            }
        }

        if( Ele_TriggerMatched == false && Mu_TriggerMatched == false ) continue;
//	cnt1++;
        if( EventTag > 2 ) continue;
//	cnt2++;
//	if (fabs(dPhi_ll) > 2.7) {
//	if( EventTag == 0) cntemu0++;
//	if( EventTag == 1) cntetau0++;
//	if( EventTag == 2) cntmutau0++;
//	}
//
//	if ((fabs(dPhi_ll) > 2.7)&& Jet_isGoodB) {
//	if( EventTag == 0) cntemu1++;
//	if( EventTag == 1) cntetau1++;
//	if( EventTag == 2) cntmutau1++;
//	}

        cnt0++;
//	cout <<dPhi_ll<<endl;
        if(fabs(dPhi_ll) < 2.7) continue;
//	cnt3++;

//	if(
//        if( MET_Et > 30 ) continue;
        weight = EventWeight;
        Etag = EventTag;
        xsecSF = xsec;
        NormSF = CalNMSF;

//            weight = EventWeight;

//        bool GBJ = 0;
//        if(EventTag == 0) {
//	for(size_t pp= 0; pp<(Jet_isGoodB)->size();pp++){
//		if ((*Jet_isGoodB)[pp]) GBJ =1;
//	}
//	if (GBJ ==1) continue;
//	}
//	cnt1++;

        for (size_t jj=0; jj<(Ele_ID_Medium)->size(); jj++) {
//            if (EventTag == 0) {
//                if (!(((*Ele_ID_Medium)[jj] == 1)||((((*Ele_ID_Tight)[jj] == 1)&&((*Ele_Iso)[jj] == 1))))) continue;
//                if (!(((*Ele_ID_Medium)[jj] == 1))) continue;
//            }
//            if ((EventTag == 1)||(EventTag == 2)) {
            if (!((((*Ele_ID_Tight)[jj] == 1)&&((*Ele_Iso)[jj] == 1)))) continue;
//            }

            ele_pt = (*Ele_pt)[jj];
            ele_eta = (*Ele_eta)[jj];
            ele_phi = (*Ele_phi)[jj];
            ele_isTight = (*Ele_isTight)[jj];
	    if (!isData){
            electronL1CaloSF = (*Ele_L1CaloSF)[jj];
            electronIDSF = (*Ele_IDSF)[jj];
            electronIsoSF = (*Ele_IsoSF)[jj];
            electronRecoSF = (*Ele_RecoSF)[jj];
            electronTrigSF = (*Ele_TrigEff)[jj];
	    }
        }

        for (size_t kk=0; kk<(Mu_pt)->size(); kk++) {
            if(!((*Mu_d0pass)[kk])) continue;
            if(!((*Mu_z0pass)[kk])) continue;
            if(((*Mu_isOverlap)[kk])) continue;
            if ((EventTag ==0)||(EventTag == 2)) {
                if(!(*Mu_isHighPt)[kk]) continue;
//		if (EventTag == 2){
                if(!((*Mu_Iso)[kk])) continue;
            }
            if(EventTag ==1) {
                if(!(*Mu_isHighPt)[kk]) continue;
                if(!((*Mu_Iso)[kk])) continue;
            }

            mu_pt = (*Mu_pt)[kk];
            mu_eta = (*Mu_eta)[kk];
            mu_phi = (*Mu_phi)[kk];
            mu_isTight = (*Mu_isHighPt)[kk];
	    if (!isData){
            muonRecoSF = ((*Mu_RecoSF)[kk]);
            muonIsoSF = ((*Mu_IsoSF)[kk]);
            muonTtvaSF = ((*Mu_TrackSF)[kk]);
            muonTrigSF = ((*Mu_TrigSF)[kk]);
	    }
        }

        for (size_t ll=0; ll<(Tau_pt)->size(); ll++) {
            if (!(*TauSelector)[ll]) continue;
            if (!(*TauSelector_WithOLR)[ll]) continue;
            if ((*Tau_isOverlap)[ll]) continue;

            tau_pt = (*Tau_pt)[ll];
            tau_eta = (*Tau_eta)[ll];
            tau_phi = (*Tau_phi)[ll];
            tau_isTight = (*TauSelector_WithOLR)[ll];
	    if(!isData){
            tauRecoSF =(*Tau_RecoSF)[0];
            tauEJetIDSF = (*Tau_IDSF)[0];
            tauEleOlrSF = (*Tau_EleOLRSF)[0];
	    }
        }

        for (size_t mm= 0; mm<(Jet_JVTSF)->size(); mm++) {
            jetJvtSF *= (*Jet_JVTSF)[mm];
        }
        if (isData) {
            weight = 1;
        } else {
            if( EventTag == 0) weight = PrwWeight*MCWeight* muonTrigSF*muonRecoSF*muonIsoSF*muonTtvaSF*electronTrigSF*electronIDSF*electronRecoSF*electronIsoSF*jetJvtSF;
            if( EventTag == 1) weight = PrwWeight*MCWeight*electronTrigSF*electronIDSF*electronRecoSF*electronIsoSF*tauRecoSF*tauEleOlrSF*tauEJetIDSF*jetJvtSF;
            if( EventTag == 2) weight = PrwWeight*MCWeight*muonTrigSF*muonRecoSF*muonIsoSF*muonTtvaSF*tauRecoSF*tauEleOlrSF*tauEJetIDSF*jetJvtSF;
        }




        if(Etag == 0) {
            DPhi_ll = CaldPhi(ele_phi, mu_phi);
        }
        if(Etag == 1) {
            DPhi_ll = CaldPhi(ele_phi, tau_phi);
//	    cout <<weight<<endl;
//	     if (weight < 0.001){
//		     cout << "PrwWeight"<<PrwWeight<<endl;
//		     cout << "electronIDSF"<<electronIDSF<<endl;
//		     cout << "electronRecoSF"<<electronRecoSF<<endl;
//		     cout << "electronRecoSF"<<electronRecoSF<<endl;
//		     cout << "tauEleOlrSF"<<tauEleOlrSF<<endl;
//		     cout << "tauRecoSF"<<tauRecoSF<<endl;
//		     cout << "tauEJetIDSF"<<tauEJetIDSF<<endl;
//		     cout << "MCWeight"<<MCWeight<<endl;
//		     cout << "electronTrigSF"<<electronTrigSF<<endl;
//		     cout << "electronIsoSF"<<electronIsoSF<<endl;
//		     cout << "electronL1CaloSF"<<electronL1CaloSF<<endl;
//		     cout << "jetJvtSF"<<jetJvtSF<<endl;
//	     }


        }
        if(Etag == 2) {
            DPhi_ll = CaldPhi(mu_phi, tau_phi);
        }
//        cout << DPhi_ll <<endl;

        EMuTauTree->Fill();
    }

    cout<<"     "<<endl;
    cout << cnt0 << endl;
    cout << cnt1 << endl;
//    cout << cnt2 << endl;
//    cout << cnt3 << endl;
//    cout << cntemu << endl;
//    cout << cntetau << endl;
//    cout << cntmutau << endl;
//
//    cout << cntemu0 << endl;
//    cout << cntetau0 << endl;
//    cout << cntmutau0 << endl;
//    cout << cntemu1 << endl;
//    cout << cntetau1 << endl;
//    cout << cntmutau1 << endl;

    ntupleF->cd();
    EMuTauTree->Write();
    ntupleF->Close();
//   delete ntupleF;
//   delete EMuTauTree;
}


void NtupleMaker::RunSkim() {
    TChain *ssTree = new TChain("LFV");
    meta = new TChain("metaTree");
    typedef std::vector<std::string>::iterator STRING_ITOR;
    STRING_ITOR inF;
    string INFName;
    for (inF = InputFileList.begin(); inF != InputFileList.end(); ++inF) {
        cout<<*inF<<endl;
	INFName = *inF;
        ssTree->Add((*inF).c_str());
        meta->Add((*inF).c_str());
    }
    luminosity = Lum(INFName);
    cout <<"lum"<<luminosity<<endl;
    cout<<"mcCA"<<mcChannelNumber<<endl;
    cout<<OutPutName<<endl;
    ReadTreeLFVO(ssTree);

    SkimProcessor(OutPutName,ssTree);
    delete ssTree;
    delete meta;

}
