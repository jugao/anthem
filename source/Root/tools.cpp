#include "tools.h"
#include <iomanip>

string TMPN;
string m_time;
string m_chan;

void ploteps(TCanvas* canvas) {
    char str[80];
    strcpy (str,canvas->GetName());
    strcat (str,".eps");
    canvas->Print(str);
}

void plotgif(TCanvas* canvas) {
    char str[80];
    strcpy (str,canvas->GetName());
    strcat (str,".gif");
    canvas->Print(str);
}
void plotGraph(string xx, vector<string>  yy) {
    ifstream infxx, infyy;
    infxx.open(xx.c_str());
    vector<double> Plotx;
    vector<double> Ploty;
    double Tx;
    double Ty;
    std::cout << "debug0" <<std::endl;
    while ( infxx >> Tx) {
        cout <<Tx<<endl;
        Plotx.push_back(Tx);
    }
    TCanvas *c1 = new TCanvas("test","test", 900,900);
    c1->cd();
    short Clor[7] = {kRed,kBlue,kYellow,kGreen,kGray,kOrange,kBlack};
    int PN = yy.size();
    int xN = Plotx.size();
    TGraphErrors *plots[PN];
    int couter = -1;
    std::cout << "debug1" <<std::endl;
    std::cout << PN <<std::endl;
    double RangeMax = 0;
    for (auto & Fy : yy) {
        std::cout << Fy <<std::endl;
        couter++;
        infyy.open(Fy.c_str());
        while(infyy >> Ty) {
            Ploty.push_back(Ty);
            cout <<Ty<<endl;
	    if (RangeMax < Ty) RangeMax = Ty;
        }
	Fy.erase(Fy.length()-9);

        plots[couter] = new TGraphErrors(xN, &(Plotx[0]), &(Ploty[0]),0,0);
        plots[couter]->SetMarkerStyle(20);
        plots[couter]->SetMarkerSize(1);
        plots[couter]->SetMarkerColor(Clor[couter]);
        plots[couter]->SetLineColor(Clor[couter]);
        plots[couter]->SetLineWidth(2);
        if (couter == 0) {
	    plots[couter]->GetYaxis()->SetRangeUser(0,RangeMax*1.5);
            plots[couter] ->Draw("LPA");
            plots[couter] ->SetTitle(Fy.c_str());
        } else {
         plots[couter] ->SetTitle(Fy.c_str());
 
            plots[couter] ->Draw("LP");
        }
        Ploty.clear();
        infyy.close();

    }
	gPad->BuildLegend(0.65,0.78,0.88,0.88);
    ploteps(c1);
    plotgif(c1);

}



void myText(Double_t x,Double_t y,Color_t color, const char *text, Double_t tsize) {

    TLatex l;
    l.SetTextAlign(12);
    l.SetTextSize(tsize);
    l.SetTextFont(42);
    l.SetNDC();
    l.SetTextColor(color);
    l.DrawLatex(x,y,text);
}

void ATLASLabel(Double_t x,Double_t y,const char* text,Color_t color) {
    TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);
    l.SetNDC();
    l.SetTextFont(72);
    l.SetTextColor(color);

    double delx = 0.1*696*gPad->GetWh()/(472*gPad->GetWw());

    l.DrawLatex(x,y,"ATLAS");
    if (text) {
        TLatex p;
        p.SetNDC();
        p.SetTextFont(42);
        p.SetTextColor(color);
        p.DrawLatex(x+delx,y,text);
        //    p.DrawLatex(x,y,"#sqrt{s}=900GeV");
    }
}


/**
 * Convert a double into a string
 */
std::string convertDouble(double number) {
    std::stringbuf buffer;
    std::ostream os (&buffer);
    os << number;
    return buffer.str();
}

/**
 * Convert a int into a string
 */

std::string convertInt(int number) {
    std::stringstream ss;//create a stringstream
    ss << number;//add number to the stream
    return ss.str();//return a string with the contents of the stream
}

/**
 * Calculate luminosity normalization
 */

double CalNorm(double lumiData, double xSec,double sumW) {
    double r = lumiData/(sumW/xSec);
    return r;
}

/**
 * ATLAS STYLE
 */
void MyStyle() {
    TStyle* atlasStyle = new TStyle("ATLAS","Atlas style");
    // use plain black on white colors
//    Int_t icol=0; // WHITE
//    atlasStyle->SetFrameBorderMode(icol);
//    atlasStyle->SetFrameFillColor(icol);
//    atlasStyle->SetCanvasBorderMode(icol);
//    atlasStyle->SetCanvasColor(icol);
//    atlasStyle->SetPadBorderMode(icol);
//    atlasStyle->SetPadColor(icol);
//    atlasStyle->SetStatColor(icol);
    //atlasStyle->SetFillColor(icol); // don't use: white fill color for *all* objects

    // set the paper & margin sizes
//    atlasStyle->SetPaperSize(20,26);
//
//    // set margin sizes
//    atlasStyle->SetPadTopMargin(0.05);
//    atlasStyle->SetPadRightMargin(0.05);
//    atlasStyle->SetPadBottomMargin(0.16);
//    atlasStyle->SetPadLeftMargin(0.16);
//
//    // set title offsets (for axis label)
//    atlasStyle->SetTitleXOffset(1.4);
//    atlasStyle->SetTitleYOffset(1.4);
//
//    // use large fonts
//    //Int_t font=72; // Helvetica italics
//    Int_t font=42; // Helvetica
//    Double_t tsize=0.04;
//    atlasStyle->SetTextFont(font);
//
//    atlasStyle->SetTextSize(tsize);
//    atlasStyle->SetLabelFont(font,"x");
//    atlasStyle->SetTitleFont(font,"x");
//    atlasStyle->SetLabelFont(font,"y");
//    atlasStyle->SetTitleFont(font,"y");
//    atlasStyle->SetLabelFont(font,"z");
//    atlasStyle->SetTitleFont(font,"z");
//    atlasStyle->SetHistLineWidth(2);
//
//    atlasStyle->SetLabelSize(tsize,"x");
//    atlasStyle->SetTitleSize(tsize,"x");
//    atlasStyle->SetLabelSize(tsize,"y");
//    atlasStyle->SetTitleSize(tsize,"y");
//    atlasStyle->SetLabelSize(tsize,"z");
//    atlasStyle->SetTitleSize(tsize,"z");
//
//
//  // get rid of X error bars
//  //atlasStyle->SetErrorX(0.001);
//  // get rid of error bar caps
////  atlasStyle->SetEndErrorSize(0.);
//
//  // do not display any of the standard histogram decorations
////  atlasStyle->SetOptTitle(0);
////  //atlasStyle->SetOptStat(1111);
////  atlasStyle->SetOptStat(0);
////  //atlasStyle->SetOptFit(1111);
////  atlasStyle->SetOptFit(0);
//
//  // put tick marks on top and RHS of plots
//  atlasStyle->SetPadTickX(1);
//  atlasStyle->SetPadTickY(1);
//
//
////    // use bold lines and markers
////    atlasStyle->SetMarkerStyle(20);
////    atlasStyle->SetMarkerSize(1.2);
////    atlasStyle->SetHistLineWidth(2.);
//    atlasStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes
////
////    // get rid of X error bars and y error bar caps
////    //atlasStyle->SetErrorX(0.001);
////
////    // do not display any of the standard histogram decorations
////    atlasStyle->SetOptTitle(0);
////    //atlasStyle->SetOptStat(1111);
////    atlasStyle->SetOptStat(0);
////    //atlasStyle->SetOptFit(1111);
////    atlasStyle->SetOptFit(0);
////    // put tick marks on top and RHS of plots
////    atlasStyle->SetPadTickX(1);
////    atlasStyle->SetPadTickY(1);
////
////    // set margin sizes
//    atlasStyle->SetPadTopMargin(0.1);
//    atlasStyle->SetPadRightMargin(0.1);
//    atlasStyle->SetPadBottomMargin(0.1);
//    atlasStyle->SetPadLeftMargin(0.1);
//    // set title offsets (for axis label)
//    atlasStyle->SetTitleXOffset(1.);
//    atlasStyle->SetTitleYOffset(1.);
//
//    atlasStyle->SetOptTitle(1);
//    atlasStyle->SetTitleFillColor(10);
//    atlasStyle->SetTitleBorderSize(0);
//
//    atlasStyle->SetPalette(1);
//    atlasStyle->SetOptStat(0);
//    atlasStyle->SetStatBorderSize(1);
//    atlasStyle->SetTitleBorderSize(0);
//    atlasStyle->SetPadBorderMode(0);
//    // atlasStyle->SetPadRightMargin(0.15);
//    atlasStyle->SetCanvasBorderMode(0);
////  atlasStyle->SetStats(0);
//
//    atlasStyle->SetTimeOffset(1199145600);
//
    gROOT->SetStyle("ATLAS");
    gROOT->ForceStyle();
}

Plot::Plot(char *fch,char *Cch,int CL, int CW ) {
    fName = fch ;
    CanvasName = Cch ;
    CanvasWidth = CL;
    CanvasLength = CW;
    TH1::SetDefaultSumw2();
}

//void Plot::plot2D(TH1D* th1, TH1D* th2) {
//    c1 = new TCanvas(fName,fName, CanvasWidth,CanvasLength );
//    c1->cd();
//    TH2D * hcoll = new TH2D("hcol1","",th1->GetNbinsX(),th1->GetBinLowEdge(1),th1->GetBinLowEdge(th1->GetNbinsX()+1),th2->GetNbinsX(),th2->GetBinLowEdge(1),th2->GetBinLowEdge(th2->GetNbinsX()+1));
//
//
//}


void Plot::plotDataMCSTA( TH1D *data, THStack *mc,TLegend *legend,vector<TH1D*> & vecT ) {

    c1 = new TCanvas(fName,fName, CanvasWidth,CanvasLength );
    c1->cd();
    pad1 = new TPad("pad1", "pad1",0,0.15,1,1);
    pad3 = new TPad("pad3", "pad3",0,0,1,0.23);
    pad1->Draw();
    pad1->cd();
//    TH1D *mcD = (TH1D*)mc->GetStack()->Last();
//    data->Draw("hist l");
    mc->Draw("hist");
    mc->SetMinimum(0.1);
    mc->SetMaximum(1400000);
    mc->GetXaxis()->SetLabelOffset(0.3);
    mc->GetYaxis()->SetTitle("Events");
    mc->GetYaxis()->SetTitleSize(0.04);
    mc->GetYaxis()->SetTitleOffset(1.1);
    TH1D *mcD = (TH1D*)mc->GetStack()->Last();
    short kclor = kRed;
//    data->Draw("C SAME");
//    data->Draw("sameE");
    legend-> SetNColumns(2);
//    legend->AddEntry(data,"DATA","lep");
    legend->Draw("same");
    legend->SetLineColor(0);
    data->SetMarkerStyle(20);
    data->SetLineWidth(2);
    for (auto& tt : vecT) {
        tt->Draw("hist same l");
        tt->SetLineStyle(2);
        tt->SetLineWidth(2);
        tt->SetLineColor(kclor);
        tt->SetMarkerColor(kclor);
        legend->AddEntry(tt,tt->GetName(),"lep");
        kclor = kclor - 2;
    }
    pad1->SetLogy();

    myText(0.20,0.79,1,("#sqrt{#it{s}} = 13 TeV, #scale[0.6]{#int}#it{L}d#it{t} ="+LatexInfo[m_time]).c_str(),0.04);
    TLatex * tex = new TLatex(0.2,0.74,m_chan.c_str());
    tex->SetNDC();
    tex->SetTextFont(42);
    tex->SetTextSize(0.04);
    tex->SetLineWidth(2);
    tex->Draw();
    ATLASLabel(0.20,0.84,"Internal",1);


    c1->cd();
    pad3->Draw();
    pad3->cd();
    pad3->SetBottomMargin(0.3424242);
    TH1D *ratio=new TH1D("ratio","",data->GetNbinsX(),data->GetBinLowEdge(1),data->GetBinLowEdge(data->GetNbinsX()+1));
    TH1D *ratio0=new TH1D("ratio0","",data->GetNbinsX(),data->GetBinLowEdge(1),data->GetBinLowEdge(data->GetNbinsX()+1));
    double Nmc = 0;
    double Ndata = 0 ;
    double NdataMax = 0;
    for(Long64_t j=1; j<(data->GetNbinsX()+1); j++ ) {
        Ndata = data->GetBinContent(j);
        Nmc = mcD->GetBinContent(j);
        ratio0->SetBinContent(j,1);
        if (Nmc== 0) continue;
        if (Ndata== 0) continue;
        ratio->SetBinContent(j,Ndata/Nmc);
        ratio->SetBinError(j,Ndata/Nmc*sqrt(1/Ndata+1/Nmc));
        if (NdataMax < Ndata)   NdataMax = Ndata;
    }
    ratio0->SetLineColor(kRed);
    mc->SetMaximum((NdataMax)*100  );
    ratio->SetMinimum(0.5);
    ratio->SetMaximum(1.5);
    ratio->GetXaxis()->SetLabelFont(42);
    ratio->GetXaxis()->SetLabelSize(0.12);
    ratio->GetYaxis()->SetLabelSize(0.12);
    ratio->GetYaxis()->SetTitle("Data/MC");
    ratio->GetXaxis()->SetTitle((VBFHZZXTitle[TMPN.c_str()]).c_str());
    ratio->GetYaxis()->SetTitleSize(0.14);
    ratio->GetXaxis()->SetTitleSize(0.14);
    ratio->GetYaxis()->SetTitleOffset(0.3);
    ratio->GetYaxis()->SetNdivisions(505);
    ratio->GetXaxis()->SetTitleOffset(1.1);
    ratio->SetMarkerStyle(20);
    ratio->SetLineColor(kBlack);
    ratio->SetLineWidth(2);
    ratio->SetStats(0);
    ratio->Draw("E");
    ratio0->Draw("same");
    ratio->SetStats(0);
    ratio0->SetStats(0);
    pad3->SetGridy();
    pad1->SetTickx(1);
    pad1->SetTicky(1);
    pad3->SetTickx(1);
    pad3->SetTicky(1);
    ploteps(c1);
    plotgif(c1);

}
void Plot::plotDataMCSTA( TH1D *data, THStack *mc,TLegend *legend ) {
//     MyStyle();
    c1 = new TCanvas(fName,fName, CanvasWidth,CanvasLength );
    c1->cd();
    pad1 = new TPad("pad1", "pad1",0,0.15,1,1);
    pad3 = new TPad("pad3", "pad3",0,0,1,0.23);
    pad1->Draw();
    pad1->cd();
//    TH1D *mcD = (TH1D*)mc->GetStack()->Last();
//    data->Draw("hist l");
    mc->Draw("hist");
    mc->SetMinimum(0.1);
    mc->SetMaximum(1400000);
    mc->GetXaxis()->SetLabelOffset(0.3);
    mc->GetYaxis()->SetTitle("Events");
    mc->GetYaxis()->SetTitleSize(0.04);
    mc->GetYaxis()->SetTitleOffset(1.1);
    TH1D *mcD = (TH1D*)mc->GetStack()->Last();
//    data->Draw("C SAME");
    data->Draw("sameE");
    legend-> SetNColumns(2);
    legend->AddEntry(data,"DATA","lep");
    legend->Draw("same");
    legend->SetLineColor(0);
    data->SetMarkerStyle(20);
    data->SetLineWidth(2);
    pad1->SetLogy();

    myText(0.20,0.79,1,("#sqrt{#it{s}} = 13 TeV, #scale[0.6]{#int}#it{L}d#it{t} ="+LatexInfo[m_time]).c_str(),0.04);
    TLatex * tex = new TLatex(0.2,0.74,m_chan.c_str());
    tex->SetNDC();
    tex->SetTextFont(42);
    tex->SetTextSize(0.04);
    tex->SetLineWidth(2);
    tex->Draw();
    ATLASLabel(0.20,0.84,"Internal",1);




    c1->cd();
    pad3->Draw();
    pad3->cd();
    pad3->SetBottomMargin(0.3424242);
    TH1D *ratio=new TH1D("ratio","",data->GetNbinsX(),data->GetBinLowEdge(1),data->GetBinLowEdge(data->GetNbinsX()+1));
    TH1D *ratio0=new TH1D("ratio0","",data->GetNbinsX(),data->GetBinLowEdge(1),data->GetBinLowEdge(data->GetNbinsX()+1));
    double Nmc = 0;
    double Ndata = 0 ;
    double NdataMax = 0;
    for(Long64_t j=1; j<(data->GetNbinsX()+1); j++ ) {
        Ndata = data->GetBinContent(j);
        Nmc = mcD->GetBinContent(j);
        ratio0->SetBinContent(j,1);
        if (Nmc== 0) continue;
        if (Ndata== 0) continue;
        ratio->SetBinContent(j,Ndata/Nmc);
        ratio->SetBinError(j,Ndata/Nmc*sqrt(1/Ndata+1/Nmc));
        if (NdataMax < Ndata)   NdataMax = Ndata;
    }
    ratio0->SetLineColor(kRed);
    mc->SetMaximum((NdataMax)*100  );
    ratio->SetMinimum(0.5);
    ratio->SetMaximum(1.5);
    ratio->GetXaxis()->SetLabelFont(42);
    ratio->GetXaxis()->SetLabelSize(0.12);
    ratio->GetYaxis()->SetLabelSize(0.12);
    ratio->GetYaxis()->SetTitle("Data/MC");
    ratio->GetXaxis()->SetTitle((VBFHZZXTitle[TMPN.c_str()]).c_str());
    ratio->GetYaxis()->SetTitleSize(0.14);
    ratio->GetXaxis()->SetTitleSize(0.14);
    ratio->GetYaxis()->SetTitleOffset(0.3);
    ratio->GetYaxis()->SetNdivisions(505);
    ratio->GetXaxis()->SetTitleOffset(1.1);
    ratio->SetMarkerStyle(20);
    ratio->SetLineColor(kBlack);
    ratio->SetLineWidth(2);
    ratio->SetStats(0);
    ratio->Draw("E");
    ratio0->Draw("same");
    ratio->SetStats(0);
    ratio0->SetStats(0);
    pad3->SetGridy();
    pad1->SetTickx(1);
    pad1->SetTicky(1);
    pad3->SetTickx(1);
    pad3->SetTicky(1);
    ploteps(c1);
    plotgif(c1);

}


void Plot::plotDataMC(TH1D* data, TH1D* mc) {
    c1 = new TCanvas(fName, fName, CanvasWidth,CanvasLength );
    c1->cd();
    pad1 = new TPad("pad1", "pad1",0,0.32,1,1);
    pad3 = new TPad("pad3", "pad3",0,0.2,1,0.38);
    pad1->Draw();
    pad1->cd();
    mc->Scale(data->Integral()/mc->Integral());
    mc->Draw("hist");
    mc->SetMinimum(0.1);
    data->Draw("sameE");
    c1->cd();
    pad3->Draw();
    pad3->cd();
    pad3->SetBottomMargin(0.3424242);

    ratio=new TH1D("ratio","",data->GetNbinsX(),data->GetBinLowEdge(1),data->GetBinLowEdge(data->GetNbinsX()+1));
    ratio0=new TH1D("ratio0","",data->GetNbinsX(),data->GetBinLowEdge(1),data->GetBinLowEdge(data->GetNbinsX()+1));
    double Nmc = 0;
    double Ndata = 0 ;
    double NdataMax = 0;
    for(Long64_t j=1; j<(data->GetNbinsX()+1); j++ ) {
        Ndata = data->GetBinContent(j);
        Nmc = mc->GetBinContent(j);
        ratio0->SetBinContent(j,1);
        if (Nmc== 0) continue;
        if (Ndata== 0) continue;
        ratio->SetBinContent(j,Ndata/Nmc);
        ratio->SetBinError(j,Ndata/Nmc*sqrt(1/Ndata+1/Nmc));
        if (NdataMax < Ndata)   NdataMax = Ndata;
    }
    ratio0->SetLineColor(kRed);
    mc->SetMaximum((NdataMax)*1.5  );
    ratio->SetMinimum(0.95);
    ratio->SetMaximum(1.05);
//    ratio->GetXaxis()->SetLabelFont(42);
    ratio->GetYaxis()->SetLabelSize(0.11);
    ratio->GetXaxis()->SetLabelSize(0.12);
    ratio->GetYaxis()->SetTitle("Data/MC");
    ratio->GetXaxis()->SetTitle("m^{CC}_{ee}[GeV]");
    ratio->GetYaxis()->SetTitleSize(0.15);
    ratio->GetXaxis()->SetTitleSize(0.15);
    ratio->GetYaxis()->SetTitleOffset(0.3);
    ratio->GetXaxis()->SetTitleOffset(1.1);
    ratio->SetMarkerStyle(20);
    ratio->SetLineColor(kBlack);
    ratio->SetLineWidth(2);
//    ratio->SetLineWidth(0);
    ratio->SetStats(0);
    ratio->Draw("E");
    ratio0->Draw("same");
    ratio->SetStats(0);
    ratio0->SetStats(0);
    pad3->SetGridy();
    pad1->SetTickx(1);
    pad1->SetTicky(1);
    pad3->SetTickx(1);
    pad3->SetTicky(1);
//	delete ratio;
//	delete ratio0;
//	delete c1;
//	delete pad3;
}


SetBin::SetBin(int bin1, double lowe1, double highe1,int bin2, double lowe2, double highe2){
    nBins = bin1;
    lowEdge = lowe1;
    highEdge = highe1;
    nBins2 = bin2;
    lowEdge2 = lowe2;
    highEdge2 = highe2;

}


SetBin::SetBin(int bin, double lowe,double highe) {
    nBins = bin;
    lowEdge = lowe;
    highEdge = highe;
}

SetBin::SetBin(int bin, double *binarr) {
    nBins = bin;
    arr = binarr;
}

MakeHist::MakeHist(string ObjName, string SName) {
    TH1::SetDefaultSumw2();
    OName = ObjName;
    SSName = SName;
    Binning["LFVMll"] = new SetBin(30,0,600);
    Binning["LFVlePt"] = new SetBin(30,0,1200);
    Binning["LFVleEta"] = new SetBin(16,-2.5,2.5);
    Binning["LFVdilePt"] = new SetBin(20,0,1200);
    Binning["LFVlePhi"] = new SetBin(32,-3.2,3.2);
    Binning["LFVdileRap"] = new SetBin(12,-4.2,4.2);
    Binning["LFVMjj"] = new SetBin(40,0,2000);
    Binning["LFVDeltaEtajj"] = new SetBin(40,0,8);
    Binning["LFVMET"] = new SetBin(50,0,500);
    Binning["LFVMETSig"] = new SetBin(35,0,35);
    Binning["LFVNumJets"] = new SetBin(14,0,14);
    Binning["LFVPtLead"] = new SetBin(50,0,500);
    Binning["LFVPtSub"] = new SetBin(50,0,500);
    Binning["LFVlep1Pt"] = new SetBin(30,0,1200);
    Binning["LFVlep2Pt"] = new SetBin(30,0,1200);
    Binning["LFVlep1Phi"] = new SetBin(32,-3.2,3.2);
    Binning["LFVlep2Phi"] = new SetBin(32,-3.2,3.2);
    Binning["LFVlep1Eta"] = new SetBin(16,-2.5,2.5);
    Binning["LFVlep2Eta"] = new SetBin(16,-2.5,2.5);

    Binning["Set"] = new SetBin(SNbin,SNL,SNH);

    //inclusive cut Bin
    Binning["VBFHMllvvMjj"] = new SetBin(40,0,2000);
    Binning["VBFHMllvvDeltaEtajj"] = new SetBin(40,0,8);
    Binning["VBFHMllvvMET"] = new SetBin(50,0,500);
    Binning["VBFHMllvvMETSig"] = new SetBin(35,0,35);
    Binning["VBFHMllvvNumJets"] = new SetBin(14,0,14);
    Binning["VBFHMllvvNumBJets"] = new SetBin(14,0,14);
    Binning["VBFHMllvvPtLead"] = new SetBin(50,0,500);
    Binning["VBFHMllvvPtSub"] = new SetBin(50,0,500);
    Binning["VBFHMllvvMll"] = new SetBin(40,70,110);
    Binning["VBFHMllvvlePt"] = new SetBin(30,0,1200);
    Binning["VBFHMllvvleEta"] = new SetBin(16,-2.5,2.5);
    Binning["VBFHMllvvdilePt"] = new SetBin(20,0,1200);
    Binning["VBFHMllvvdMetZPhi"] = new SetBin(32,0,3.2);
    Binning["VBFHMllvvdPhiJ100met"] = new SetBin(32,0,3.2);
    Binning["VBFHMllvvdileRap"] = new SetBin(42,0,4.2);
    Binning["VBFHMllvvDvide"] = new SetBin(10,0,1);
    Binning["VBFHMllvvPFractPt"] = new SetBin(10,0,1);
    Binning["VBFHMllvvPMetOHT"] = new SetBin(10,0,1);
    Binning["VBFHMllvvlep3Pt"] = new SetBin(30,0,1200);
    Binning["VBFHMllvvlep3Eta"] = new SetBin(16,-2.5,2.5);
    Binning["VBFHMllvvlep3Phi"] = new SetBin(32,0,3.2);
    Binning["VBFHMllvvMWT"] = new SetBin(50,0,500);
    Binning["VBFHMllvvZMT"] = new SetBin(40,100,1700);

    //VBF cut Bin
    Binning["AFVBFHMllvvMjj"] = new SetBin(40,0,2000);
    Binning["AFVBFHMllvvDeltaEtajj"] = new SetBin(40,0,8);
    Binning["AFVBFHMllvvMET"] = new SetBin(50,0,500);
    Binning["AFVBFHMllvvMETSig"] = new SetBin(35,0,35);
    Binning["AFVBFHMllvvNumJets"] = new SetBin(14,0,14);
    Binning["AFVBFHMllvvNumBJets"] = new SetBin(14,0,14);
    Binning["AFVBFHMllvvPtLead"] = new SetBin(50,0,500);
    Binning["AFVBFHMllvvPtSub"] = new SetBin(50,0,500);
    Binning["AFVBFHMllvvMll"] = new SetBin(40,70,110);
    Binning["AFVBFHMllvvlePt"] = new SetBin(30,0,1200);
    Binning["AFVBFHMllvvleEta"] = new SetBin(16,-2.5,2.5);
    Binning["AFVBFHMllvvdilePt"] = new SetBin(20,0,1200);
    Binning["AFVBFHMllvvdMetZPhi"] = new SetBin(32,0,3.2);
    Binning["AFVBFHMllvvdPhiJ100met"] = new SetBin(32,0,3.2);
    Binning["AFVBFHMllvvdileRap"] = new SetBin(42,0,4.2);
    Binning["AFVBFHMllvvDvide"] = new SetBin(10,0,1);
    Binning["AFVBFHMllvvPFractPt"] = new SetBin(10,0,1);
    Binning["AFVBFHMllvvPMetOHT"] = new SetBin(10,0,1);
    Binning["AFVBFHMllvvlep3Pt"] = new SetBin(30,0,1200);
    Binning["AFVBFHMllvvlep3Eta"] = new SetBin(16,-2.5,2.5);
    Binning["AFVBFHMllvvlep3Phi"] = new SetBin(32,0,3.2);
    Binning["AFVBFHMllvvMWT"] = new SetBin(50,0,500);
    Binning["AFVBFHMllvvZMT"] = new SetBin(50,0,500);
    Binning["2DMETDeltajj"] = new SetBin(50,0,500,40,0,8);
    Binning["2DMETMjj"] = new SetBin(50,0,500,40,0,2000);
    Binning["2DMETSigMjj"] = new SetBin(35,0,35,40,0,2000);
    Binning["2DMETSigDeltajj"] = new SetBin(35,0,35,40,0,8);

}

void MakeHist::MKCutFlow() {

    CutFlow1 = new TH1D((OName+"CutFlow1").c_str(),"",20,0,20);
    CutFlow2 = new TH1D((OName+"CutFlow2").c_str(),"",10,0,10);
    CutFlow3 = new TH1D((OName+"CutFlow3").c_str(),"",10,0,10);
    CutFlow4 = new TH1D((OName+"CutFlow4").c_str(),"",10,0,10);
    CutFlow5 = new TH1D((OName+"CutFlow5").c_str(),"",10,0,10);
}

void MakeHist::ReadCutFlowSum( TH1D *cut,string Cate ) {
    vector<string> vec;
    if ((OName.find("VBF3lCR")) != string::npos) {
        vec.push_back("3lepton      ");
        vec.push_back("Type         ");
        vec.push_back("lep3rd ID    ");
        vec.push_back("lep3rd Pt>20 ");
        vec.push_back("MWt > 60     ");
        vec.push_back("Bjet veto    ");
        vec.push_back("Njets >= 2   ");
        vec.push_back("2jet pt > 30 ");
    } else  if ((OName.find("VBFHMllvv")) != string::npos) {
        vec.push_back("Type             ");
        vec.push_back("event_3CR = 0    ");
        vec.push_back("MET > 120        ");
        vec.push_back("|dLepR| < 1.8    ");
        vec.push_back("|dMetZPhi| > 2.7 ");
        vec.push_back("|dMetJPhi| > 0.4 ");
        vec.push_back("Bjet Veto        ");
        vec.push_back("frac_pT < 0.2    ");
        vec.push_back("MetOHT > 0.4     ");
        vec.push_back("MetSig > 11      ");
        vec.push_back("Njets > 2        ");
        vec.push_back("2jet pt > 30     ");
        vec.push_back("Mjj > 550        ");
        vec.push_back("|DeltaEtajj|>4.4 ");

    }
    fstream ScaleSmear_out;
    ScaleSmear_out.open((Cate+".txt").c_str(),ios::app);
    size_t ii = vec.size();
    ScaleSmear_out << setiosflags(ios::fixed)<<setprecision(2)<< cut->GetBinContent(ii)<< "+/-"<<setiosflags(ios::fixed)<<setprecision(2)<<cut->GetBinError(ii)  <<endl;
}
void MakeHist::ReadCutFlow( TH1D *cut,string Cate ) {
    vector<string> vec;
    if ((OName.find("VBF3lCR")) != string::npos) {
        vec.push_back("3lepton      ");
        vec.push_back("Type         ");
        vec.push_back("lep3rd ID    ");
        vec.push_back("lep3rd Pt>20 ");
        vec.push_back("MWt > 60     ");
        vec.push_back("Bjet veto    ");
        vec.push_back("Njets >= 2   ");
        vec.push_back("2jet pt > 30 ");
    } else  if ((OName.find("VBFHMllvv")) != string::npos) {
        vec.push_back("Type             ");
        vec.push_back("event_3CR = 0    ");
        vec.push_back("MET > 120        ");
        vec.push_back("|dLepR| < 1.8    ");
        vec.push_back("|dMetZPhi| > 2.7 ");
        vec.push_back("|dMetJPhi| > 0.4 ");
        vec.push_back("Bjet Veto        ");
        vec.push_back("frac_pT < 0.2    ");
        vec.push_back("MetOHT > 0.4     ");
        vec.push_back("MetSig > 11      ");
        vec.push_back("Njets > 2        ");
        vec.push_back("2jet pt > 30     ");
        vec.push_back("Mjj > 550        ");
        vec.push_back("|DeltaEtajj|>4.4 ");
    }



    fstream ScaleSmear_out;
    ScaleSmear_out.open((OName+SSName+".txt").c_str(),ios::app);
    for ( size_t ii = 1;  ii<(vec.size()+1); ii++)
        if (ii !=1) {
            ScaleSmear_out << vec[ii-1]<<":"<< cut->GetBinContent(ii)<< "+/-"<<cut->GetBinError(ii)  << " ; " <<(cut->GetBinContent(ii)/cut->GetBinContent(1)) <<" ; " <<(cut->GetBinContent(ii)/(cut->GetBinContent(ii-1)))<<endl;
        } else {
            ScaleSmear_out << Cate <<":"<<endl;
            ScaleSmear_out << vec[ii-1]<<":"<< cut->GetBinContent(ii)<< "+/-"<<cut->GetBinError(ii)  << " ; " <<(cut->GetBinContent(ii)/cut->GetBinContent(1)) <<" ; " <<(cut->GetBinContent(ii)/(cut->GetBinContent(ii)))<<endl;
//            ScaleSmear_out << vec[ii-1]<<":"<< cut->GetBinContent(ii) << " ; " <<(cut->GetBinContent(ii)/cut->GetBinContent(1)) <<" ; " <<(cut->GetBinContent(ii)/(cut->GetBinContent(ii)))<<endl;
        }
    ScaleSmear_out << " "<<endl;
    ScaleSmear_out.close();
}

void MakeHist::MKTH1D() {
    string TName;
    if ((OName == "EMu" )|| (OName == "ETau") || (OName == "MuTau") || ((OName.find("LFV")) != string::npos ) || ((OName.find("EMu")) != string::npos ) || ((OName.find("ETau")) != string::npos ) || ((OName.find("MuTau")) != string::npos )  ) {
        TName = "LFV";
        for (auto& x : LFVHist ) {
            TH1D *h = new TH1D((OName+x.second).c_str(),"",Binning[(TName+x.second)]->nBins,Binning[(TName+x.second).c_str()]->lowEdge,Binning[(TName+x.second).c_str()]->highEdge);
            THList.push_back(h);
        }

    }
    if (((OName.find("VBFHMllvv")) != string::npos) || ((OName.find("VBF3lCR")) != string::npos)) {
        TName = "VBFHMllvv";

        for (auto& x : VBFHist ) {
            TH1D *h = new TH1D((OName+x.second).c_str(),"",Binning[(TName+x.second)]->nBins,Binning[(TName+x.second).c_str()]->lowEdge,Binning[(TName+x.second).c_str()]->highEdge);
//	    cout<<(OName+x.second).c_str()<<endl;
            THList.push_back(h);
        }

    }


}

void MakeHist::MKTH2D() {
	for (auto& x:VBFHist2D ) {
		TH2D *h = new TH2D((OName+x.second).c_str(),"",Binning[("2D"+x.second)]->nBins,Binning[("2D"+x.second)]->lowEdge,Binning[("2D"+x.second)]->highEdge,Binning[("2D"+x.second)]->nBins2,Binning[("2D"+x.second)]->lowEdge2,Binning[("2D"+x.second)]->highEdge2);
		THList2D.push_back(h);
	}
}


void MakeHist::DeleteTH1D() {
    if ((OName.find("VBFHMllvv")) != string::npos ) {
        delete dileRap;
        delete Mjj;
        delete DeltaEtajj;
        delete MET;
        delete METSig;
        delete NumJets;
        delete NumBJets;
        delete PtLead;
        delete PtSub;
    }



}


MakeTHStack::MakeTHStack(string ObjName,string SName):MakeHist(ObjName,SName) {

    legend = new TLegend(0.65,0.68,0.88,0.88);
    if (((OName.find("VBFHMllvv")) != string::npos) || ((OName.find("VBF3lCR")) != string::npos)) {
        for (auto& x : VBFHist ) {
            THStack *S = new THStack((x.second).c_str(),"");
            THStackVBF.push_back(S);
        }

    }
    if ((OName == "EMu" )|| (OName == "ETau") || (OName == "MuTau") || ((OName.find("LFV")) != string::npos ) || ((OName.find("EMu")) != string::npos ) || ((OName.find("ETau")) != string::npos ) || ((OName.find("MuTau")) != string::npos )  ) {
        for (auto& x : LFVHist ) {
            THStack *S = new THStack((x.second).c_str(),"");
            THStackLFV.push_back(S);
        }

    }
}

void MakeTHStack::ReadFile(string FileName) {
    short m_color;
    TFile * Input = new TFile(FileName.c_str(),"READ");

    if((FileName.find("Top") != string::npos)) m_color = kGray;
    if((FileName.find("ZZ") != string::npos)) m_color = kAzure;
    if((FileName.find("WZ") != string::npos)) m_color = kRed;
    if((FileName.find("WW") != string::npos)) m_color = kGreen;
    if((FileName.find("Wt") != string::npos)) m_color = kViolet;
    if((FileName.find("Zjets") != string::npos)) m_color = kYellow;
    if((FileName.find("Other") != string::npos)) m_color = kOrange;

    if((FileName.find("Diboson") != string::npos)) m_color = kOrange;
//    if((FileName.find("Top") != string::npos)) m_color = kRed;
    if((FileName.find("Wjets") != string::npos)) m_color = kYellow;
    if((FileName.find("DYll") != string::npos)) m_color = kViolet;
    if((FileName.find("LMass") != string::npos)) m_color = kAzure;


    if (((OName.find("VBFHMllvv")) != string::npos) || ((OName.find("VBF3lCR")) != string::npos)) {
        for (size_t ii =0; ii<THStackVBF.size(); ii++ ) {
            TH1D *h  = (TH1D*)Input->Get((OName+VBFHist[ii]).c_str());
            h->SetFillColor(m_color);
            h->SetLineWidth(0);
            h->SetMinimum();
            THStackVBF[ii]->Add(h);
            THList.push_back(h);
        }
    }

    if ((OName == "EMu" )|| (OName == "ETau") || (OName == "MuTau") || ((OName.find("LFV")) != string::npos ) || ((OName.find("EMu")) != string::npos ) || ((OName.find("ETau")) != string::npos ) || ((OName.find("MuTau")) != string::npos )  ) {
        for (size_t ii =0; ii<THStackLFV.size(); ii++ ) {
            TH1D *h  = (TH1D*)Input->Get((OName+LFVHist[ii]).c_str());
            h->SetFillColor(m_color);
            h->SetLineWidth(0);
            h->SetMinimum();
            THStackLFV[ii]->Add(h);
            THList.push_back(h);
        }
    }



    if((FileName.find("Top") != string::npos)) legend->AddEntry( THList[0],"Top","F");
    if((FileName.find("ZZ") != string::npos)) legend->AddEntry( THList[0],"ZZ","F");
    if((FileName.find("WZ") != string::npos)) legend->AddEntry( THList[0],"WZ","F");
    if((FileName.find("WW") != string::npos)) legend->AddEntry( THList[0],"WW","F");
    if((FileName.find("Wt") != string::npos)) legend->AddEntry( THList[0],"Wt","F");
    if((FileName.find("Zjets") != string::npos)) legend->AddEntry( THList[0],"Zjets","F");
    if((FileName.find("Other") != string::npos)) legend->AddEntry( THList[0],"Other","F");
    if((FileName.find("Diboson") != string::npos)) legend->AddEntry( THList[0],"Diboson","F");
    if((FileName.find("DY") != string::npos)) legend->AddEntry( THList[0],"DYll","F");
//    if((FileName.find("Top") != string::npos)) legend->AddEntry( THList[0],"Top","F");
    if((FileName.find("Wjets") != string::npos)) legend->AddEntry( THList[0],"Wjets","F");
    if((FileName.find("LMass") != string::npos)) legend->AddEntry( THList[0],"Zll","F");

    THList.clear();
//    Input->Close();
//    delete Input;
}

void MakeTHStack::DrawStack(string dataFile,string DrawOption,bool drawSignal) {
    if( (dataFile.find("1516A17")) != string::npos ) {
        m_time = "1516A17";
    } else if( (dataFile.find("15161718")) != string::npos ) {
        m_time = "15161718";
    } else if( (dataFile.find("17")) != string::npos ) {
        m_time = "17";
    } else if( (dataFile.find("1516")) != string::npos ) {
        m_time = "1516";
    }


    if ( (OName.find("AllCh"))!= string::npos && (OName.find("VBFHMllvv")!= string::npos )) {
        m_chan = "#mu#mu+ee";
    } else if( (OName.find("AllCh"))!= string::npos && (OName.find("VBF3lCR")!= string::npos )) {
        m_chan = "#mu#mue+#mu#mu#mu+eee+ee#mu";
    } else if ( (OName.find("mumue"))!= string::npos ) {
        m_chan = "#mu#mue";
    } else if ( (OName.find("mumumu"))!= string::npos ) {
        m_chan = "#mu#mu#mu";
    } else if ( (OName.find("eemu"))!= string::npos ) {
        m_chan = "ee#mu";
    } else if ( (OName.find("eee"))!= string::npos ) {
        m_chan = "eee";
    } else if ( (OName.find("ee"))!= string::npos ) {
        m_chan = "ee";
    } else if ( (OName.find("mumu"))!= string::npos ) {
        m_chan = "#mu#mu";
    } else if ((OName.find("EMu")) != string::npos ) {
        m_chan = "e#mu";
    } else if ((OName.find("ETau")) != string::npos ) {
        m_chan = "e#tau";
    } else if ((OName.find("MuTau")) != string::npos ) {
        m_chan = "#mu#tau";
    }

    TFile *da = new TFile(dataFile.c_str(),"READ");
    TH1D *drawDa = (TH1D*)da->Get((OName+DrawOption).c_str());
    string TT = OName+DrawOption;
    Plot *VBFDraw = new Plot((char*)((TT).c_str()),(char*)((TT).c_str()),900,900);
    TMPN = DrawOption;
//    MCStack[DrawOption.c_str()]->Draw("hist");
    int kk = -1;
    if (((OName.find("VBFHMllvv")) != string::npos) || ((OName.find("VBF3lCR")) != string::npos)) {
        for (auto& x : VBFHist ) {
            if ( x.second == DrawOption) kk = x.first;
        }
        if (drawSignal) {
            VBFDraw->plotDataMCSTA(drawDa,THStackVBF[kk],legend,SGD);
        } else {
            VBFDraw->plotDataMCSTA(drawDa,THStackVBF[kk],legend);
        }
    }
    
    if ((OName == "EMu" )|| (OName == "ETau") || (OName == "MuTau") || ((OName.find("LFV")) != string::npos ) || ((OName.find("EMu")) != string::npos ) || ((OName.find("ETau")) != string::npos ) || ((OName.find("MuTau")) != string::npos )  ) {

        for (auto& x : LFVHist ) {
            if ( x.second == DrawOption) kk = x.first;
        }
	if((DrawOption.find("lep")) != string::npos){
          if((OName.find("EMu")) != string::npos ) TMPN = "EMu"+TMPN;
          if((OName.find("ETau")) != string::npos ) TMPN = "ETau"+TMPN;
          if((OName.find("MuTau")) != string::npos ) TMPN = "MuTau"+TMPN;

	}

            VBFDraw->plotDataMCSTA(drawDa,THStackLFV[kk],legend);
    }


}



TH1D *MakeHist::DrawDilp(string option,string binName, TLorentzVector l1, TLorentzVector l2,double weight ) {
    TH1D *UndefPlot = new TH1D(option.c_str(),option.c_str(),Binning[binName]->nBins,Binning[binName]->lowEdge,Binning[binName]->highEdge);
    if (option == "mass") {
        double mass = (l1+l2).M();
        UndefPlot->Fill(mass,weight);
    }
    return UndefPlot;
}
