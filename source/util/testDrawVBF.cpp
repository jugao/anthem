#include "tools.h"


using namespace std;

int main(int argc, char* argv[] ) {
    string dataSample = argv[1];
    string chtype = argv[2]; //channel type ee,mumu...
    string DrawOb = argv[3]; //Draw object, MET Mass....
    string CateG = argv[4]; // Categorty, VBF, 3lCR
    string Sglist = argv[5];//Signal list

//	TApplication *myapp=new TApplication("myapp",0,0);
    string CCG;
    if ((CateG.find("3lCR") != string::npos)) CCG = "VBF3lCRT";
    else if ((CateG.find("VBF") != string::npos)) CCG ="VBFHMllvvT";

    if (Sglist.find("Signal")!=string::npos) {
        ifstream Li1;
        ifstream Li2;
        Li1.open(argv[2]);
        Li2.open(argv[3]);
        string Ty;
        string TD;
        vector<string> Do;
        while(Li2 >> TD) {
            Do.push_back(TD);
        }
        while(Li1 >> Ty) {
            for (auto & xx : Do) {
                cout<<Ty<<endl;
                cout<<xx<<endl;
                MakeTHStack *M1 = new MakeTHStack((CCG+Ty).c_str(),"TT");
                ifstream SGL;
                SGL.open(argv[5]);
                string TF;
                while (SGL >> TF) {
                    TFile *sg = new TFile(TF.c_str(),"READ");
                    TH1D *h1 = (TH1D*)sg->Get((CCG+Ty+xx).c_str());
                    h1->SetName((TF.erase(TF.length()-10)).c_str());
                    M1->SGD.push_back(h1);
                }
                if (CCG == "VBF3lCRT") {
                    M1->ReadFile("ZZPlots.root");
                    M1->ReadFile("WtPlots.root");
                    M1->ReadFile("OtherPlots.root");
                    M1->ReadFile("TopPlots.root");
                    M1->ReadFile("WWPlots.root");
                    M1->ReadFile("ZjetsPlots.root");
                    M1->ReadFile("WZPlots.root");
                    M1->DrawStack(dataSample,xx,1);
                } else {
                    M1->ReadFile("BKGALL.root");

//                    M1->ReadFile("OtherPlots.root");
//                    M1->ReadFile("WtPlots.root");
//                    M1->ReadFile("WWPlots.root");
//                    M1->ReadFile("ZZPlots.root");
//                    M1->ReadFile("WZPlots.root");
//                    M1->ReadFile("TopPlots.root");
//                    M1->ReadFile("ZjetsPlots.root");
                    M1->DrawStack(dataSample,xx,1);

                }
            }
        }
    } else if ((chtype.find("list") != string::npos)&& (DrawOb.find("list") != string::npos)) {
        ifstream Li1;
        ifstream Li2;
        Li1.open(argv[2]);
        Li2.open(argv[3]);
        string Ty;
        string TD;
        vector<string> Do;
        while(Li2 >> TD) {
            Do.push_back(TD);
        }
        while(Li1 >> Ty) {
            for (auto & xx : Do) {
                cout<<Ty<<endl;
                cout<<xx<<endl;
                MakeTHStack *M1 = new MakeTHStack((CCG+Ty).c_str(),"TT");
                if (CCG == "VBF3lCRT") {
                    M1->ReadFile("ZZPlots.root");
                    M1->ReadFile("WtPlots.root");
                    M1->ReadFile("OtherPlots.root");
                    M1->ReadFile("TopPlots.root");
                    M1->ReadFile("WWPlots.root");
                    M1->ReadFile("ZjetsPlots.root");
                    M1->ReadFile("WZPlots.root");
                    M1->DrawStack(dataSample,xx);
                } else {
//                    M1->ReadFile("BKGALL.root");

                    M1->ReadFile("OtherPlots.root");
                    M1->ReadFile("WtPlots.root");
                    M1->ReadFile("WWPlots.root");
                    M1->ReadFile("ZZPlots.root");
                    M1->ReadFile("WZPlots.root");
                    M1->ReadFile("TopPlots.root");
                    M1->ReadFile("ZjetsPlots.root");
                    M1->DrawStack(dataSample,xx);

                }
            }
        }
    } else if(DrawOb == "CutFlow") {
        ifstream Li1;
        Li1.open(argv[2]);
        TFile *da = new TFile(dataSample.c_str(),"READ");
        string Ty;
        dataSample.erase(dataSample.length()-5);
        MakeTHStack *M1 = new MakeTHStack((dataSample+CCG).c_str(),"");
        while(Li1 >> Ty) {
            cout<<Ty<<endl;
            TH1D *h1 = (TH1D*)da->Get((CCG+Ty+"CutFlow1").c_str());
            cout << (CCG+Ty+"CutFlow1").c_str()<<endl;
            M1->ReadCutFlow(h1,Ty);
        }
    } else if(DrawOb == "CutFlowSum") {
        ifstream Li1;
        ifstream Li2;
        Li1.open(argv[2]);
        Li2.open(argv[1]);
        string Ty;
        string TD;
        vector<string> Do;
        while(Li2 >> TD) {
            Do.push_back(TD);
        }
        while(Li1 >> Ty) {
            for (auto & xx : Do) {
                TFile *da = new TFile(xx.c_str(),"READ");
                MakeTHStack *M1 = new MakeTHStack((xx+CCG).c_str(),"");
                cout<<Ty<<endl;
                TH1D *h1 = (TH1D*)da->Get((CCG+Ty+"CutFlow1").c_str());
                cout << (CCG+Ty+"CutFlow1").c_str()<<endl;
                M1->ReadCutFlowSum(h1,Ty);
            }
        }
    } else {
        MakeTHStack *M1 = new MakeTHStack((CCG+chtype).c_str(),"TT");
        if (CCG == "VBF3lCRT") {
            M1->ReadFile("ZZPlots.root");
            M1->ReadFile("WtPlots.root");
            M1->ReadFile("OtherPlots.root");
            M1->ReadFile("TopPlots.root");
            M1->ReadFile("WWPlots.root");
            M1->ReadFile("ZjetsPlots.root");
            M1->ReadFile("WZPlots.root");
            M1->DrawStack(dataSample,DrawOb);
        } else {
            M1->ReadFile("OtherPlots.root");
            M1->ReadFile("WtPlots.root");
            M1->ReadFile("WWPlots.root");
            M1->ReadFile("ZZPlots.root");
            M1->ReadFile("WZPlots.root");
            M1->ReadFile("TopPlots.root");
            M1->ReadFile("ZjetsPlots.root");
            M1->DrawStack(dataSample,DrawOb);
        }
    }


//	myapp->Run();

    return 0;
}
