#include "tools.h"

using namespace std;

int main(int argc, char* argv[] ) {

//    TApplication *myapp=new TApplication("myapp",0,0);
    string OPtion = argv[3] ;
    if ((argc == 4) && (OPtion == "Graph")) {
        string Fx = argv[1];
        string FyL = argv[2];
        vector<string> Do;
        string TD;
        ifstream Li;
        Li.open(FyL.c_str());
        while (Li>>TD) {
            Do.push_back(TD);
            cout <<TD<<endl;
        }
        plotGraph(Fx,Do);
    }
    if (argc == 5) {
        string dataSample = argv[1];
        string chtype = argv[2]; //channel type ee,mumu...
        string DrawOb = argv[3]; //Draw object, MET Mass....
        string CateG = argv[4]; // Categorty, CR...
        string CCG;
        if ((CateG.find("SR") != string::npos)) CCG = "LFVMHT";
        if ((chtype.find("list") != string::npos)&& (DrawOb.find("list") != string::npos)) {
            ifstream Li1;
            ifstream Li2;
            Li1.open(argv[2]);
            Li2.open(argv[3]);
            string Ty;
            string TD;
            vector<string> Do;
            while(Li2 >> TD) {
                Do.push_back(TD);
            }
            while(Li1 >> Ty) {
                for (auto & xx : Do) {
                    cout<<Ty<<endl;
                    cout<<xx<<endl;
                    MakeTHStack *M1 = new MakeTHStack((CCG+Ty).c_str(),"TT");
                    if (CCG == "LFVMHT") {
                        M1->ReadFile("DibosonPlots.root");
                        M1->ReadFile("LMassPlots.root");
//                        M1->ReadFile("DYllPlots.root");
                        M1->ReadFile("WjetsPlots.root");
                        M1->ReadFile("TopPlots.root");
                        M1->DrawStack(dataSample,xx);


                    }
                }
            }
        }
    }
//    myapp->Run();

    return 0;
}
